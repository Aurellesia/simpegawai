<div class="table-responsive-sm">
    <table class="table table-striped" id="pelanggaranPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
        <th>Pelanggaran</th>
        <th>Tindakan</th>
        <th>Keterangan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $pelanggaranPegawais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pelanggaranPegawai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
            <td><?php echo e($pelanggaranPegawai->id_pegawai); ?></td>
            <td><?php echo e($pelanggaranPegawai->Pelanggaran->pelanggaran); ?></td>
            <td><?php echo e($pelanggaranPegawai->Tindakan->tindakan); ?></td>
            <td><?php echo e($pelanggaranPegawai->keterangan); ?></td>
                <td>
                    <?php echo Form::open(['route' => ['pelanggaranPegawais.destroy', $pelanggaranPegawai->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('pelanggaranPegawais.show', [$pelanggaranPegawai->id])); ?>" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="<?php echo e(route('pelanggaranPegawais.edit', [$pelanggaranPegawai->id])); ?>" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        <?php echo Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<?php /**PATH C:\xampp\htdocs\simpeg\resources\views/pelanggaran__pegawais/table.blade.php ENDPATH**/ ?>