<!-- Id Pegawai Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('id_pegawai', 'Id Pegawai:'); ?>

    <?php echo Form::select('id_pegawai', $pegawaiItems, null, ['class' => 'form-control']); ?>

</div>

<!-- Gapok Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('gapok', 'Gapok:'); ?>

    <?php echo Form::select('gapok', $gapokItems, null, ['class' => 'form-control']); ?>

</div>

<!-- Gaji Tambahan Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('gaji_tambahan', 'Gaji Tambahan:'); ?>

    <?php echo Form::number('gaji_tambahan', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo e(route('penggajianPegawais.index')); ?>" class="btn btn-secondary">Cancel</a>
</div>
<?php /**PATH C:\xampp\htdocs\simpeg\resources\views/penggajian_pegawais/fields.blade.php ENDPATH**/ ?>