<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatanPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
        <th>Id Struktural</th>
        <th>Id Fungsional</th>
        <th>Id Tambahan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $jabatanPegawais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatanPegawai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($jabatanPegawai->id_pegawai); ?></td>
            <td><?php echo e($jabatanPegawai->struktural->jabatan_struktural); ?></td>
            <td><?php echo e($jabatanPegawai->fungsional->jabatan_fungsional); ?></td>
            <td><?php echo e($jabatanPegawai->tambahan->jabatan_tambahan); ?></td>
                <td>
                    <?php echo Form::open(['route' => ['jabatanPegawais.destroy', $jabatanPegawai->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('jabatanPegawais.show', [$jabatanPegawai->id])); ?>" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="<?php echo e(route('jabatanPegawais.edit', [$jabatanPegawai->id])); ?>" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        <?php echo Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<?php /**PATH C:\xampp\htdocs\simpeg\resources\views/jabatan_pegawais/table.blade.php ENDPATH**/ ?>