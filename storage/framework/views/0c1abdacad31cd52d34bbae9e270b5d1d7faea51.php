<div class="table-responsive-sm">
    <table class="table table-striped" id="penggajianPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
                <th>Gapok</th>
                <th>Gaji Tambahan</th>
                <th>Gaji Total</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $penggajianPegawais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penggajianPegawai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($penggajianPegawai->id_pegawai); ?></td>
                <td><?php echo e($penggajianPegawai->Gapok->nominal_gaji); ?></td>
                <td><?php echo e($penggajianPegawai->gaji_tambahan); ?></td>
                <td><?php echo e($penggajianPegawai->gaji_total); ?></td>
                <td>
                    <?php echo Form::open(['route' => ['penggajianPegawais.destroy', $penggajianPegawai->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('penggajianPegawais.show', [$penggajianPegawai->id])); ?>" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="<?php echo e(route('penggajianPegawais.edit', [$penggajianPegawai->id])); ?>" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        <?php echo Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div><?php /**PATH C:\xampp\htdocs\simpeg\resources\views/penggajian_pegawais/table.blade.php ENDPATH**/ ?>