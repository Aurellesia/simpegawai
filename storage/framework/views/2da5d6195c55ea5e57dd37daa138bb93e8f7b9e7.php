
<li class="nav-item <?php echo e(Request::is('pegawais*') ? 'active' : ''); ?>">
    <a class="nav-link" href="<?php echo e(route('pegawais.index')); ?>">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Pegawai</span>
    </a>
</li>
<li class="nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Master</span>
    </a>
    <ul class="nav-dropdown-items">
        <li class="nav-item <?php echo e(Request::is('agamas*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('agamas.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Agama</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('cutis*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('cutis.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Cuti</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('diklats*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('diklats.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Diklat</span>
            </a>
        </li>

        <li class="nav-item <?php echo e(Request::is('gapoks*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('gapoks.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Gaji Pokok</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('jabatanStrukturals*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('jabatanStrukturals.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Struktural</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('jabatanFunsionals*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('jabatanFunsionals.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Funsional</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('jabatanTambahans*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('jabatanTambahans.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Tambahan</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('pendidikans*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('pendidikans.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Pendidikan</span>
            </a>
        </li>
        <li class="nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <span>Master Pelanggaran</span>
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item <?php echo e(Request::is('pelanggarans*') ? 'active' : ''); ?>">
                    <a class="nav-link" href="<?php echo e(route('pelanggarans.index')); ?>">
                        <i class="nav-icon icon-cursor"></i>
                        <span>Pelanggaran</span>
                    </a>
                </li>
                <li class="nav-item <?php echo e(Request::is('tindakans*') ? 'active' : ''); ?>">
                    <a class="nav-link" href="<?php echo e(route('tindakans.index')); ?>">
                        <i class="nav-icon icon-cursor"></i>
                        <span>Tindakan</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li class="nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Transaksi</span>
    </a>
    <ul class="nav-dropdown-items">
        <li class="nav-item <?php echo e(Request::is('pelanggaranPegawais*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('pelanggaranPegawais.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Pelanggaran  Pegawai</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('absensis*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('absensis.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Absensis</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('penggajianPegawais*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('penggajianPegawais.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Penggajian Pegawais</span>
            </a>
        </li>
        <li class="nav-item <?php echo e(Request::is('jabatanPegawais*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('jabatanPegawais.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan Pegawais</span>
            </a>
        </li>

        <li class="nav-item <?php echo e(Request::is('diklatPegawais*') ? 'active' : ''); ?>">
            <a class="nav-link" href="<?php echo e(route('diklatPegawais.index')); ?>">
                <i class="nav-icon icon-cursor"></i>
                <span>Diklat Pegawais</span>
            </a>
        </li>
    </ul>
</li>

<?php /**PATH C:\xampp\htdocs\simpeg\resources\views/layouts/menu.blade.php ENDPATH**/ ?>