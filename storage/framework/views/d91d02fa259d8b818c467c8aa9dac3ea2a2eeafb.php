<table class="table table-striped" style="text-align:center;" id="absensis-table">
    <thead>
        <tr>
            <th rowspan="2">Id Pegawai</th>
            <th colspan="3">Kehadiran</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>Masuk</th>
            <th>izin</th>
            <th>tidak masuk</th>
        </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $pegawai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td>
                <?php
                $mytime = date('d/m/Y');
                ?>
                <input type="hidden" name="id_pegawai[]" value="<?php echo e($p->id); ?>">
                <input type="hidden" name="tanggal[]" value="<?php echo e($mytime); ?>">
                <?php echo e($p->nama); ?>


            </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[<?php echo e($p->id); ?>]" checked="checked" value="Masuk">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[<?php echo e($p->id); ?>]" value="Izin">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[<?php echo e($p->id); ?>]" value="tidak masuk">
            </label>
        </td>
        <td>
            <!-- Keterangan Field -->
            <div class="form-group col-sm-12">
                <?php echo Form::text('keterangan[]', null, ['class' => 'form-control']); ?>

            </div>
        </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo e(route('absensis.index')); ?>" class="btn btn-secondary">Cancel</a>
</div>
<?php /**PATH C:\xampp\htdocs\simpeg\resources\views/absensis/fields.blade.php ENDPATH**/ ?>