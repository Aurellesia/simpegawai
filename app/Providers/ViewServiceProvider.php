<?php

namespace App\Providers;
use App\Models\Diklat;






use App\Models\Jabatan_Tambahan;
use App\Models\Jabatan_Funsional;
use App\Models\Jabatan_Struktural;
use App\Models\Gapok;
use App\Models\Tindakan;
use App\Models\Pelanggaran;
use App\Models\Pegawai;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['diklat_pegawais.fields'], function ($view) {
            $diklatItems = Diklat::pluck('jenis_diklat','id')->toArray();
            $view->with('diklatItems', $diklatItems);
        });
        View::composer(['diklat_pegawais.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        View::composer(['jabatan_pegawais.fields'], function ($view) {
            $jabatan__tambahanItems = Jabatan_Tambahan::pluck('jabatan_tambahan','id')->toArray();
            $view->with('jabatan__tambahanItems', $jabatan__tambahanItems);
        });
        View::composer(['jabatan_pegawais.fields'], function ($view) {
            $jabatan__funsionalItems = Jabatan_Funsional::pluck('jabatan_fungsional','id')->toArray();
            $view->with('jabatan__funsionalItems', $jabatan__funsionalItems);
        });
        View::composer(['jabatan_pegawais.fields'], function ($view) {
            $jabatan__strukturalItems = Jabatan_Struktural::pluck('jabatan_struktural','id')->toArray();
            $view->with('jabatan__strukturalItems', $jabatan__strukturalItems);
        });
        View::composer(['jabatan_pegawais.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        View::composer(['penggajian_pegawais.fields'], function ($view) {
            $gapokItems = Gapok::pluck('masa_kerja','id')->toArray();
            $view->with('gapokItems', $gapokItems);
        });
        View::composer(['penggajian_pegawais.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        View::composer(['absensis.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        View::composer(['pelanggaran__pegawais.fields'], function ($view) {
            $tindakanItems = Tindakan::pluck('tindakan','id')->toArray();
            $view->with('tindakanItems', $tindakanItems);
        });
        View::composer(['pelanggaran__pegawais.fields'], function ($view) {
            $pelanggaranItems = Pelanggaran::pluck('pelanggaran','id')->toArray();
            $view->with('pelanggaranItems', $pelanggaranItems);
        });
        View::composer(['pelanggaran__pegawais.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        View::composer(['pelanggaran_pegawais.fields'], function ($view) {
            $pelanggaranItems = Pelanggaran::pluck('pelanggaran','id')->toArray();
            $view->with('pelanggaranItems', $pelanggaranItems);
        });
        View::composer(['pelanggaran_pegawais.fields'], function ($view) {
            $pegawaiItems = Pegawai::pluck('nama','id')->toArray();
            $view->with('pegawaiItems', $pegawaiItems);
        });
        //
    }
}
