<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJabatanPegawaiRequest;
use App\Http\Requests\UpdateJabatanPegawaiRequest;
use App\Repositories\JabatanPegawaiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class JabatanPegawaiController extends AppBaseController
{
    /** @var  JabatanPegawaiRepository */
    private $jabatanPegawaiRepository;

    public function __construct(JabatanPegawaiRepository $jabatanPegawaiRepo)
    {
        $this->jabatanPegawaiRepository = $jabatanPegawaiRepo;
    }

    /**
     * Display a listing of the JabatanPegawai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jabatanPegawais = $this->jabatanPegawaiRepository->all();

        return view('jabatan_pegawais.index')
            ->with('jabatanPegawais', $jabatanPegawais);
    }

    /**
     * Show the form for creating a new JabatanPegawai.
     *
     * @return Response
     */
    public function create()
    {
        return view('jabatan_pegawais.create');
    }

    /**
     * Store a newly created JabatanPegawai in storage.
     *
     * @param CreateJabatanPegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreateJabatanPegawaiRequest $request)
    {
        $input = $request->all();

        $jabatanPegawai = $this->jabatanPegawaiRepository->create($input);

        Flash::success('Jabatan Pegawai saved successfully.');

        return redirect(route('jabatanPegawais.index'));
    }

    /**
     * Display the specified JabatanPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jabatanPegawai = $this->jabatanPegawaiRepository->find($id);

        if (empty($jabatanPegawai)) {
            Flash::error('Jabatan Pegawai not found');

            return redirect(route('jabatanPegawais.index'));
        }

        return view('jabatan_pegawais.show')->with('jabatanPegawai', $jabatanPegawai);
    }

    /**
     * Show the form for editing the specified JabatanPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jabatanPegawai = $this->jabatanPegawaiRepository->find($id);

        if (empty($jabatanPegawai)) {
            Flash::error('Jabatan Pegawai not found');

            return redirect(route('jabatanPegawais.index'));
        }

        return view('jabatan_pegawais.edit')->with('jabatanPegawai', $jabatanPegawai);
    }

    /**
     * Update the specified JabatanPegawai in storage.
     *
     * @param int $id
     * @param UpdateJabatanPegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJabatanPegawaiRequest $request)
    {
        $jabatanPegawai = $this->jabatanPegawaiRepository->find($id);

        if (empty($jabatanPegawai)) {
            Flash::error('Jabatan Pegawai not found');

            return redirect(route('jabatanPegawais.index'));
        }

        $jabatanPegawai = $this->jabatanPegawaiRepository->update($request->all(), $id);

        Flash::success('Jabatan Pegawai updated successfully.');

        return redirect(route('jabatanPegawais.index'));
    }

    /**
     * Remove the specified JabatanPegawai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jabatanPegawai = $this->jabatanPegawaiRepository->find($id);

        if (empty($jabatanPegawai)) {
            Flash::error('Jabatan Pegawai not found');

            return redirect(route('jabatanPegawais.index'));
        }

        $this->jabatanPegawaiRepository->delete($id);

        Flash::success('Jabatan Pegawai deleted successfully.');

        return redirect(route('jabatanPegawais.index'));
    }
}
