<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AppBaseController;
use App\Models\Pegawai;
use App\Models\Absensi;
use App\Models\JabatanPegawai;
use App\Models\PenggajianPegawai;
use App\Models\Pelanggaran_Pegawai;
use App\Models\DiklatPegawai;
use Flash;
use Illuminate\Support\Facades\Auth;

use App\Repositories\CutiRepository;
use App\Http\Requests\CreateCutiPegawaiRequest;
use App\Http\Requests\UpdateCutiPegawaiRequest;
use App\Models\CutiPegawai;
use App\Repositories\CutiPegawaiRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $cutiPegawaiRepository;
    private $cutiRepository;

    public function __construct(CutiRepository $cutiRepo, CutiPegawaiRepository $cutiPegawaiRepo)
    {
        $this->cutiPegawaiRepository = $cutiPegawaiRepo;
        $this->cutiRepository = $cutiRepo;
    }

    public function profile()
    {
        $id = Auth::user()->id;
        $pegawai = Pegawai::all()->where('id_user', "=", $id);
        $jabatanPegawais = JabatanPegawai::all()->where('id_pegawai', "=", $id);
        $penggajianPegawais = PenggajianPegawai::all()->where('id_pegawai', "=", $id);
        $pelanggaranPegawais = Pelanggaran_Pegawai::all()->where('id_pegawai', "=", $id);
        $diklatPegawais = DiklatPegawai::all()->where('id_pegawai', "=", $id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect('/users/profil');
        }

        return view('user_profile.index', ['diklatPegawais' => $diklatPegawais, 'pelanggaranPegawais' => $pelanggaranPegawais, 'pegawai' => $pegawai, 'jabatanPegawais' => $jabatanPegawais, 'penggajianPegawai' => $penggajianPegawais]);
    }

    public function absensi()
    {
        $id = Auth::user()->id;
        $idPegawai = Pegawai::where('id_user', $id)->first()->id;
        $absensiPegawais = Absensi::where('id_pegawai', "=", $idPegawai)->get();
        if (empty($absensiPegawais)) {
            Flash::error('Absensi not found');

            return redirect('/users/absensi');
        }
        return view('user_absensi.index', ['absensiPegawais' => $absensiPegawais]);
    }

    public function gaji()
    {
        $id = Auth::user()->id;
        $idPegawai = Pegawai::where('id_user', $id)->first()->id;
        $gajiPegawais = PenggajianPegawai::where('id_pegawai', "=", $idPegawai)->get();

        if (empty($gajiPegawais)) {
            Flash::error('Gaji not found');

            return redirect('/users/gaji');
        }
        return view('user_gaji.index', ['gajiPegawais' => $gajiPegawais]);
    }

    public function pelanggaran()
    {
        $id = Auth::user()->id;
        $idPegawai = Pegawai::where('id_user', $id)->first()->id;
        $pelanggaranPegawais = Pelanggaran_Pegawai::where('id_pegawai', "=", $idPegawai)->get();

        if (empty($pelanggaranPegawais)) {
            Flash::error('Jabatan not found');
            return redirect('/users/pelanggaran');
        }
        return view('user_pelanggaran.index', ['pelanggaranPegawais' => $pelanggaranPegawais]);
    }

    public function jabatan()
    {
        $id = Auth::user()->id;
        $idPegawai = Pegawai::where('id_user', $id)->first()->id;
        $jabatanPegawais = JabatanPegawai::where('id_pegawai', "=", $idPegawai)->get();

        if (empty($jabatanPegawais)) {
            Flash::error('Jabatan not found');
            return redirect('/users/jabatan');
        }
        return view('user_jabatan.index', ['jabatanPegawais' => $jabatanPegawais]);
    }

    public function diklat()
    {
        $id = Auth::user()->id;
        $idPegawai = Pegawai::where('id_user', $id)->first()->id;
        $diklatPegawais = DiklatPegawai::where('id_pegawai', "=", $idPegawai)->get();

        if (empty($diklatPegawais)) {
            Flash::error('Jabatan not found');
            return redirect('/users/diklat');
        }
        return view('user_diklat.index', ['diklatPegawais' => $diklatPegawais]);
    }

    public function cuti()
    {
        $cutiPegawais = $this->cutiPegawaiRepository->all();

        return view('user_cuti.index')->with('cutiPegawais', $cutiPegawais);
    }

    public function cutiCreate()
    {
        $id = Auth::user()->id;
        $pegawai = Pegawai::where('id_user', $id)->first()->pluck('id')->first();
        $cuti = $this->cutiRepository->all()->pluck('jenis_cuti','id');
        return view('user_cuti.create', compact(['cuti', 'pegawai']));
    }

    public function cutiStore(CreateCutiPegawaiRequest $request)
    {
        $input = $request->all();
        $this->cutiPegawaiRepository->create($input);
        Flash::success('Cuti pegawai sent successfully.');
        return redirect(route('user_cuti.index'));
    }

    public function cutiEdit($id)
    {
        $cutiPegawai = $this->cutiPegawaiRepository->find($id);
        $idUser = Auth::user()->id;
        $pegawai = Pegawai::where('id_user', $idUser)->first()->pluck('id')->first();
        $cuti = $this->cutiRepository->all()->pluck('jenis_cuti','id');

        if (empty($cutiPegawai)) {
            Flash::error('Cuti not found');

            return redirect(route('user_cuti.index'));
        }

        return view('user_cuti.edit', compact(['cutiPegawai', 'pegawai', 'cuti']));
    }

    public function cutiUpdate($id, UpdateCutiPegawaiRequest $request)
    {
        $cutiPegawai = $this->cutiPegawaiRepository->find($id);

        if (empty($cutiPegawai)) {
            Flash::error('Cuti not found');

            return redirect(route('user_cuti.index'));
        }

        $cutiPegawai = $this->cutiPegawaiRepository->update($request->all(), $id);

        Flash::success('Cuti updated successfully.');

        return redirect(route('user_cuti.index'));
    }



    public function cutiShow($id)
    {
        $cutiPegawais = $this->cutiPegawaiRepository->find($id);

        if (empty($cutiPegawais)) {
            Flash::error('Cuti not found');

            return redirect(route('user_cuti.index'));
        }

        return view('user_cuti.show')->with('cutiPegawais', $cutiPegawais);
    }

    public function cutiDestroy($id)
    {
        $cutiPegawais = $this->cutiPegawaiRepository->find($id);

        if (empty($cutiPegawais)) {
            Flash::error('Cuti not found');

            return redirect(route('user_cuti.index'));
        }

        $this->cutiPegawaiRepository->delete($id);

        Flash::success('Cuti deleted successfully.');

        return redirect(route('user_cuti.index'));
    }
    public function cutiAccTampil(){
        $data=CutiPegawai::all()->where('status', "=", 'Belum Disetujui');
        return view('admin_cuti.index')->with('data', $data);
    }
    public function cutiAccUpdate(Request $request){
    	DB::table('cuti_pegawais')->where('id',$request->id)->update([
			'status'=>'Disetujui',
    	]);
    	return redirect ('/admin/cuti/');
    }
}
