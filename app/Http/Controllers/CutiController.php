<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCutiRequest;
use App\Repositories\CutiRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\UpdateCutiRequest;
use Illuminate\Http\Request;
use Flash;
use Response;

class CutiController extends Controller
{
    private $cutiRepository;

    public function __construct(CutiRepository $cutiRepo)
    {
        $this->cutiRepository = $cutiRepo;
    }

    public function index(Request $request)
    {
        $cutis = $this->cutiRepository->all();

        return view('cutis.index')->with('cutis', $cutis);
    }

    public function create()
    {
        return view('cutis.create');
    }

    public function store(CreateCutiRequest $request)
    {
        $input = $request->all();
        $this->cutiRepository->create($input);
        Flash::success('Cuti saved successfully');
        return redirect(route('cutis.index'));
    }

    public function show($id)
    {
        $cuti = $this->cutiRepository->find($id);
        if (empty($cuti)) {
            Flash::error('Diklat not found');
            return redirect(route('cutis.index'));
        }
        return view('cutis.show')->with('cuti', $cuti);
    }

    public function edit($id)
    {
        $cuti = $this->cutiRepository->find($id);

        if (empty($cuti)) {
            Flash::error('Cuti not found');
            return redirect(route('cutis.index'));
        }
        return view('cutis.edit')->with('cuti', $cuti);
    }

    public function update($id, UpdateCutiRequest $request)
    {
        $cuti = $this->cutiRepository->find($id);

        if (empty($cuti)) {
            Flash::error('Cuti not found');
            return redirect(route('cutis.index'));
        }

        $cuti = $this->cutiRepository->update($request->all(), $id);
        Flash::success('Cuti updated successfully');
        return redirect(route('cutis.index'));
    }

    public function destroy($id)
    {
        $cuti = $this->cutiRepository->find($id);
        if (empty($cuti)) {
            Flash::error('Cuti not found');
            return redirect(route('cutis.index'));
        }

        $this->cutiRepository->delete($id);
        Flash::success('Cuti deleted successfully');
        return redirect(route('cutis.index'));
    }
}
