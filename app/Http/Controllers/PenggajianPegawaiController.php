<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePenggajianPegawaiRequest;
use App\Http\Requests\UpdatePenggajianPegawaiRequest;
use App\Repositories\PenggajianPegawaiRepository;
use App\Repositories\GapokRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PenggajianPegawaiController extends AppBaseController
{
    /** @var  PenggajianPegawaiRepository */
    private $penggajianPegawaiRepository;
    private $gapokRepository;

    public function __construct(PenggajianPegawaiRepository $penggajianPegawaiRepo,GapokRepository $gapokPegawaiRepo)
    {
        $this->penggajianPegawaiRepository = $penggajianPegawaiRepo;
        $this->gapokRepository = $gapokPegawaiRepo;
    }

    /**
     * Display a listing of the PenggajianPegawai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $penggajianPegawais = $this->penggajianPegawaiRepository->all();
        $gapok = $this->gapokRepository->all();
        return view('penggajian_pegawais.index')
            ->with('penggajianPegawais', $penggajianPegawais);
    }

    /**
     * Show the form for creating a new PenggajianPegawai.
     *
     * @return Response
     */
    public function create()
    {
        return view('penggajian_pegawais.create');
    }

    /**
     * Store a newly created PenggajianPegawai in storage.
     *
     * @param CreatePenggajianPegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreatePenggajianPegawaiRequest $request)
    {
        $gapok = $this->gapokRepository->find($request->gapok);
        $penggajianPegawai = $this->penggajianPegawaiRepository->create([
            'id_pegawai' => $request->id_pegawai,
            'gapok' =>  $request->gapok,
            'gaji_tambahan' => $request->gaji_tambahan,
            'gaji_total' => $gapok->nominal_gaji+$request->gaji_tambahan
            ]);
        Flash::success('Penggajian Pegawai saved successfully.');

        return redirect(route('penggajianPegawais.index'));
    }

    /**
     * Display the specified PenggajianPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $penggajianPegawai = $this->penggajianPegawaiRepository->find($id);

        if (empty($penggajianPegawai)) {
            Flash::error('Penggajian Pegawai not found');

            return redirect(route('penggajianPegawais.index'));
        }

        return view('penggajian_pegawais.show')->with('penggajianPegawai', $penggajianPegawai);
    }

    /**
     * Show the form for editing the specified PenggajianPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $penggajianPegawai = $this->penggajianPegawaiRepository->find($id);

        if (empty($penggajianPegawai)) {
            Flash::error('Penggajian Pegawai not found');

            return redirect(route('penggajianPegawais.index'));
        }

        return view('penggajian_pegawais.edit')->with('penggajianPegawai', $penggajianPegawai);
    }

    /**
     * Update the specified PenggajianPegawai in storage.
     *
     * @param int $id
     * @param UpdatePenggajianPegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePenggajianPegawaiRequest $request)
    {
        $penggajianPegawai = $this->penggajianPegawaiRepository->find($id);

        if (empty($penggajianPegawai)) {
            Flash::error('Penggajian Pegawai not found');

            return redirect(route('penggajianPegawais.index'));
        }

        $penggajianPegawai = $this->penggajianPegawaiRepository->update($request->all(), $id);

        Flash::success('Penggajian Pegawai updated successfully.');

        return redirect(route('penggajianPegawais.index'));
    }

    /**
     * Remove the specified PenggajianPegawai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $penggajianPegawai = $this->penggajianPegawaiRepository->find($id);

        if (empty($penggajianPegawai)) {
            Flash::error('Penggajian Pegawai not found');

            return redirect(route('penggajianPegawais.index'));
        }

        $this->penggajianPegawaiRepository->delete($id);

        Flash::success('Penggajian Pegawai deleted successfully.');

        return redirect(route('penggajianPegawais.index'));
    }
}
