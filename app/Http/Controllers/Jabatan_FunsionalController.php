<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJabatan_FunsionalRequest;
use App\Http\Requests\UpdateJabatan_FunsionalRequest;
use App\Repositories\Jabatan_FunsionalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Jabatan_FunsionalController extends AppBaseController
{
    /** @var  Jabatan_FunsionalRepository */
    private $jabatanFunsionalRepository;

    public function __construct(Jabatan_FunsionalRepository $jabatanFunsionalRepo)
    {
        $this->jabatanFunsionalRepository = $jabatanFunsionalRepo;
    }

    /**
     * Display a listing of the Jabatan_Funsional.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jabatanFunsionals = $this->jabatanFunsionalRepository->all();

        return view('jabatan__funsionals.index')
            ->with('jabatanFunsionals', $jabatanFunsionals);
    }

    /**
     * Show the form for creating a new Jabatan_Funsional.
     *
     * @return Response
     */
    public function create()
    {
        return view('jabatan__funsionals.create');
    }

    /**
     * Store a newly created Jabatan_Funsional in storage.
     *
     * @param CreateJabatan_FunsionalRequest $request
     *
     * @return Response
     */
    public function store(CreateJabatan_FunsionalRequest $request)
    {
        $input = $request->all();

        $jabatanFunsional = $this->jabatanFunsionalRepository->create($input);

        Flash::success('Jabatan  Funsional saved successfully.');

        return redirect(route('jabatanFunsionals.index'));
    }

    /**
     * Display the specified Jabatan_Funsional.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jabatanFunsional = $this->jabatanFunsionalRepository->find($id);

        if (empty($jabatanFunsional)) {
            Flash::error('Jabatan  Funsional not found');

            return redirect(route('jabatanFunsionals.index'));
        }

        return view('jabatan__funsionals.show')->with('jabatanFunsional', $jabatanFunsional);
    }

    /**
     * Show the form for editing the specified Jabatan_Funsional.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jabatanFunsional = $this->jabatanFunsionalRepository->find($id);

        if (empty($jabatanFunsional)) {
            Flash::error('Jabatan  Funsional not found');

            return redirect(route('jabatanFunsionals.index'));
        }

        return view('jabatan__funsionals.edit')->with('jabatanFunsional', $jabatanFunsional);
    }

    /**
     * Update the specified Jabatan_Funsional in storage.
     *
     * @param int $id
     * @param UpdateJabatan_FunsionalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJabatan_FunsionalRequest $request)
    {
        $jabatanFunsional = $this->jabatanFunsionalRepository->find($id);

        if (empty($jabatanFunsional)) {
            Flash::error('Jabatan  Funsional not found');

            return redirect(route('jabatanFunsionals.index'));
        }

        $jabatanFunsional = $this->jabatanFunsionalRepository->update($request->all(), $id);

        Flash::success('Jabatan  Funsional updated successfully.');

        return redirect(route('jabatanFunsionals.index'));
    }

    /**
     * Remove the specified Jabatan_Funsional from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jabatanFunsional = $this->jabatanFunsionalRepository->find($id);

        if (empty($jabatanFunsional)) {
            Flash::error('Jabatan  Funsional not found');

            return redirect(route('jabatanFunsionals.index'));
        }

        $this->jabatanFunsionalRepository->delete($id);

        Flash::success('Jabatan  Funsional deleted successfully.');

        return redirect(route('jabatanFunsionals.index'));
    }
}
