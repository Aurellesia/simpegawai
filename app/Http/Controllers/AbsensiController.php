<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreateabsensiRequest;
use App\Http\Requests\UpdateabsensiRequest;
use App\Repositories\PegawaiRepository;
use App\Repositories\absensiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class absensiController extends AppBaseController
{
    /** @var  absensiRepository */
    private $absensiRepository;
    private $pegawaiRepository;

    public function __construct(absensiRepository $absensiRepo,pegawaiRepository $pegawaiRepo)
    {
        $this->absensiRepository = $absensiRepo;
        $this->pegawaiRepository = $pegawaiRepo;

    }

    /**
     * Display a listing of the absensi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

    $absensis = $this->absensiRepository->all();
        $pg = $this->pegawaiRepository->all();
        $tgl=array("januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember");
        return view('absensis.index')
            ->with('absensis', $absensis)
            ->with('pegawai', $pg)
            ->with('tanggal', $tgl);
    }
    /**
     * Show the form for creating a new absensi.
     *
     * @return Response
     */
    public function create()
    {
        $pegawai = $this->pegawaiRepository->all();

        return view('absensis.create',compact(['pegawai']));
    }

    /**
     * Store a newly created absensi in storage.
     *
     * @param CreateabsensiRequest $request
     *
     * @return Response
     */
    public function store(CreateabsensiRequest $request)
    {
        $input = $request->all();
        $pg = $this->pegawaiRepository->all();
        $i=0;
        foreach($pg as $in){
            // $id=$request->get('id_pegawai');
            $absen = $this->absensiRepository->create([
            'id_pegawai' => $request->id_pegawai[$i],
            'kehadiran' =>  $request->kehadiran[$request->id_pegawai[$i]],
            'tanggal' => $request->tanggal[$i],
            'keterangan' => $request->keterangan[$i]
            ]);
            $i++;
        }
        Flash::success('Absensi saved successfully.');

        return redirect(route('absensis.index'));
    }

    /**
     * Display the specified absensi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $absensi = $this->absensiRepository->find($id);

        if (empty($absensi)) {
            Flash::error('Absensi not found');

            return redirect(route('absensis.index'));
        }

        return view('absensis.show')->with('absensi', $absensi);
    }

    /**
     * Show the form for editing the specified absensi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $absensi = $this->absensiRepository->find($id);

        if (empty($absensi)) {
            Flash::error('Absensi not found');

            return redirect(route('absensis.index'));
        }

        return view('absensis.edit')->with('absensi', $absensi);
    }

    /**
     * Update the specified absensi in storage.
     *
     * @param int $id
     * @param UpdateabsensiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateabsensiRequest $request)
    {
        $absensi = $this->absensiRepository->find($id);

        if (empty($absensi)) {
            Flash::error('Absensi not found');

            return redirect(route('absensis.index'));
        }

        $absensi = $this->absensiRepository->update($request->all(), $id);

        Flash::success('Absensi updated successfully.');

        return redirect(route('absensis.index'));
    }

    /**
     * Remove the specified absensi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $absensi = $this->absensiRepository->find($id);

        if (empty($absensi)) {
            Flash::error('Absensi not found');

            return redirect(route('absensis.index'));
        }

        $this->absensiRepository->delete($id);

        Flash::success('Absensi deleted successfully.');

        return redirect(route('absensis.index'));
    }
}
