<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePegawaiRequest;
use App\Http\Requests\UpdatePegawaiRequest;
use App\Repositories\PegawaiRepository;
use App\Repositories\JabatanPegawaiRepository;
use App\Repositories\AgamaRepository;
use App\Repositories\PendidikanRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PenggajianPegawaiRepository;
use App\Repositories\Pelanggaran_PegawaiRepository;
use App\Repositories\DiklatPegawaiRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Flash;
use Response;
use Illuminate\Support\Facades\DB;


class PegawaiController extends AppBaseController
{
    /** @var  PegawaiRepository */
    private $pegawaiRepository;
    private $agamaRepository;
    private $pendidikanRepository;
    private $jabatanPegawaiRepository;
    private $penggajianPegawaiRepository;
    private $pelanggaranPegawaiRepository;
    private $diklatPegawaiRepository;

    public function __construct(DiklatPegawaiRepository $diklatPegawaiRepo, Pelanggaran_PegawaiRepository $pelanggaranPegawaiRepo, PenggajianPegawaiRepository $penggajianPegawaiRepo, JabatanPegawaiRepository $jabatanPegawaiRepo, PegawaiRepository $pegawaiRepo, AgamaRepository $agamaRepo, PendidikanRepository $pendidikanRepo)
    {
        $this->pegawaiRepository = $pegawaiRepo;
        $this->agamaRepository = $agamaRepo;
        $this->pendidikanRepository = $pendidikanRepo;
        $this->jabatanPegawaiRepository = $jabatanPegawaiRepo;
        $this->penggajianPegawaiRepository = $penggajianPegawaiRepo;
        $this->pelanggaranPegawaiRepository = $pelanggaranPegawaiRepo;
        $this->diklatPegawaiRepository = $diklatPegawaiRepo;
    }

    /**
     * Display a listing of the Pegawai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pegawais = $this->pegawaiRepository->all();

        return view('pegawais.index')
            ->with('pegawais', $pegawais);
    }

    /**
     * Show the form for creating a new Pegawai.
     *
     * @return Response
     */
    public function create()
    {

        $agama = $this->agamaRepository->all()->pluck('agama','id');
        $pendidikan = $this->pendidikanRepository->all()->pluck('pendidikan','id');
        return view('pegawais.create',compact(['agama', 'pendidikan']));
    }

    /**
     * Store a newly created Pegawai in storage.
     *
     * @param CreatePegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreatePegawaiRequest $request)
    {
        $user = new User;
        $user->role = 'pegawai';
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id]);
        // $peg = $this->pegawaiRepository->create($request->all());
        $file = $request->file('foto');
        $tujuan_upload = 'data_file/img';
        $nama_file = time();
        $file->move($tujuan_upload, $nama_file);

        $pegawai = $this->pegawaiRepository->create([
        'id_user'=>$request->user_id,
        'nip' => $request->nip,
        'nama' => $request->nama,
        'password' => $request->password,
        'tempat' => $request->tempat,
        'tgl_lahir' => $request->tgl_lahir,
        'telp' => $request->telp,
        'jenis_kelamin' => $request->jenis_kelamin,
        'status_pegawai' => $request->status_pegawai,
        'foto' => $nama_file,
        'id_agama' => $request->id_agama,
        'hobi' => $request->hobi,
        'id_pendidikan' => $request->id_pendidikan,
        'nama_sekolah' => $request->nama_sekolah,
        'tahun_sttb' => $request->tahun_sttb,
        'gelar_depan' => $request->gelar_depan,
        'gelar_belakang' => $request->gelar_belakang,
        'status_pernikahan' => $request->status_pernikahan]
        );
        Flash::success('Pegawai saved successfully.');

        return redirect(route('pegawais.index'));
    }

    /**
     * Display the specified Pegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pegawai = $this->pegawaiRepository->find($id);
        $jabatanPegawais = $this->jabatanPegawaiRepository->all()->where('id_pegawai', "=", $id);
        $penggajianPegawais = $this->penggajianPegawaiRepository->all()->where('id_pegawai', "=", $id);
        $pelanggaranPegawais = $this->pelanggaranPegawaiRepository->all()->where('id_pegawai', "=", $id);
        $diklatPegawais = $this->diklatPegawaiRepository->all()->where('id_pegawai', "=", $id);


        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawais.index'));
        }

        return view('pegawais.show', ['diklatPegawais' => $diklatPegawais, 'pelanggaranPegawais' => $pelanggaranPegawais, 'pegawai' => $pegawai, 'jabatanPegawais' => $jabatanPegawais, 'penggajianPegawai' => $penggajianPegawais]);
    }

    /**
     * Show the form for editing the specified Pegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pegawai = $this->pegawaiRepository->find($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawais.index'));
        }

        return view('pegawais.edit')->with('pegawai', $pegawai);
    }

    /**
     * Update the specified Pegawai in storage.
     *
     * @param int $id
     * @param UpdatePegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePegawaiRequest $request)
    {
        $pegawai = $this->pegawaiRepository->find($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawais.index'));
        }

        $pegawai = $this->pegawaiRepository->update($request->all(), $id);

        Flash::success('Pegawai updated successfully.');

        return redirect(route('pegawais.index'));
    }

    /**
     * Remove the specified Pegawai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pegawai = $this->pegawaiRepository->find($id);

        if (empty($pegawai)) {
            Flash::error('Pegawai not found');

            return redirect(route('pegawais.index'));
        }

        $this->pegawaiRepository->delete($id);

        Flash::success('Pegawai deleted successfully.');

        return redirect(route('pegawais.index'));
    }

    public function profile()
    {
        return view('user_profile.index');
    }
}
