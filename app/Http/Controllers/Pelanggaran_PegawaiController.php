<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePelanggaran_PegawaiRequest;
use App\Http\Requests\UpdatePelanggaran_PegawaiRequest;
use App\Repositories\Pelanggaran_PegawaiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Pelanggaran_PegawaiController extends AppBaseController
{
    /** @var  Pelanggaran_PegawaiRepository */
    private $pelanggaranPegawaiRepository;

    public function __construct(Pelanggaran_PegawaiRepository $pelanggaranPegawaiRepo)
    {
        $this->pelanggaranPegawaiRepository = $pelanggaranPegawaiRepo;
    }

    /**
     * Display a listing of the Pelanggaran_Pegawai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pelanggaranPegawais = $this->pelanggaranPegawaiRepository->all();

        return view('pelanggaran__pegawais.index')
            ->with('pelanggaranPegawais', $pelanggaranPegawais);
    }

    /**
     * Show the form for creating a new Pelanggaran_Pegawai.
     *
     * @return Response
     */
    public function create()
    {
        return view('pelanggaran__pegawais.create');
    }

    /**
     * Store a newly created Pelanggaran_Pegawai in storage.
     *
     * @param CreatePelanggaran_PegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreatePelanggaran_PegawaiRequest $request)
    {
        $input = $request->all();

        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->create($input);

        Flash::success('Pelanggaran  Pegawai saved successfully.');

        return redirect(route('pelanggaranPegawais.index'));
    }

    /**
     * Display the specified Pelanggaran_Pegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->find($id);

        if (empty($pelanggaranPegawai)) {
            Flash::error('Pelanggaran  Pegawai not found');

            return redirect(route('pelanggaranPegawais.index'));
        }

        return view('pelanggaran__pegawais.show')->with('pelanggaranPegawai', $pelanggaranPegawai);
    }

    /**
     * Show the form for editing the specified Pelanggaran_Pegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->find($id);

        if (empty($pelanggaranPegawai)) {
            Flash::error('Pelanggaran  Pegawai not found');

            return redirect(route('pelanggaranPegawais.index'));
        }

        return view('pelanggaran__pegawais.edit')->with('pelanggaranPegawai', $pelanggaranPegawai);
    }

    /**
     * Update the specified Pelanggaran_Pegawai in storage.
     *
     * @param int $id
     * @param UpdatePelanggaran_PegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePelanggaran_PegawaiRequest $request)
    {
        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->find($id);

        if (empty($pelanggaranPegawai)) {
            Flash::error('Pelanggaran  Pegawai not found');

            return redirect(route('pelanggaranPegawais.index'));
        }

        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->update($request->all(), $id);

        Flash::success('Pelanggaran  Pegawai updated successfully.');

        return redirect(route('pelanggaranPegawais.index'));
    }

    /**
     * Remove the specified Pelanggaran_Pegawai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pelanggaranPegawai = $this->pelanggaranPegawaiRepository->find($id);

        if (empty($pelanggaranPegawai)) {
            Flash::error('Pelanggaran  Pegawai not found');

            return redirect(route('pelanggaranPegawais.index'));
        }

        $this->pelanggaranPegawaiRepository->delete($id);

        Flash::success('Pelanggaran  Pegawai deleted successfully.');

        return redirect(route('pelanggaranPegawais.index'));
    }
}
