<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGapokRequest;
use App\Http\Requests\UpdateGapokRequest;
use App\Repositories\GapokRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GapokController extends AppBaseController
{
    /** @var  GapokRepository */
    private $gapokRepository;

    public function __construct(GapokRepository $gapokRepo)
    {
        $this->gapokRepository = $gapokRepo;
    }

    /**
     * Display a listing of the Gapok.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $gapoks = $this->gapokRepository->all();

        return view('gapoks.index')
            ->with('gapoks', $gapoks);
    }

    /**
     * Show the form for creating a new Gapok.
     *
     * @return Response
     */
    public function create()
    {
        return view('gapoks.create');
    }

    /**
     * Store a newly created Gapok in storage.
     *
     * @param CreateGapokRequest $request
     *
     * @return Response
     */
    public function store(CreateGapokRequest $request)
    {
        $input = $request->all();

        $gapok = $this->gapokRepository->create($input);

        Flash::success('Gapok saved successfully.');

        return redirect(route('gapoks.index'));
    }

    /**
     * Display the specified Gapok.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gapok = $this->gapokRepository->find($id);

        if (empty($gapok)) {
            Flash::error('Gapok not found');

            return redirect(route('gapoks.index'));
        }

        return view('gapoks.show')->with('gapok', $gapok);
    }

    /**
     * Show the form for editing the specified Gapok.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gapok = $this->gapokRepository->find($id);

        if (empty($gapok)) {
            Flash::error('Gapok not found');

            return redirect(route('gapoks.index'));
        }

        return view('gapoks.edit')->with('gapok', $gapok);
    }

    /**
     * Update the specified Gapok in storage.
     *
     * @param int $id
     * @param UpdateGapokRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGapokRequest $request)
    {
        $gapok = $this->gapokRepository->find($id);

        if (empty($gapok)) {
            Flash::error('Gapok not found');

            return redirect(route('gapoks.index'));
        }

        $gapok = $this->gapokRepository->update($request->all(), $id);

        Flash::success('Gapok updated successfully.');

        return redirect(route('gapoks.index'));
    }

    /**
     * Remove the specified Gapok from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gapok = $this->gapokRepository->find($id);

        if (empty($gapok)) {
            Flash::error('Gapok not found');

            return redirect(route('gapoks.index'));
        }

        $this->gapokRepository->delete($id);

        Flash::success('Gapok deleted successfully.');

        return redirect(route('gapoks.index'));
    }
}
