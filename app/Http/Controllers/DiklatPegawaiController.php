<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDiklatPegawaiRequest;
use App\Http\Requests\UpdateDiklatPegawaiRequest;
use App\Repositories\DiklatPegawaiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DiklatPegawaiController extends AppBaseController
{
    /** @var  DiklatPegawaiRepository */
    private $diklatPegawaiRepository;

    public function __construct(DiklatPegawaiRepository $diklatPegawaiRepo)
    {
        $this->diklatPegawaiRepository = $diklatPegawaiRepo;
    }

    /**
     * Display a listing of the DiklatPegawai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $diklatPegawais = $this->diklatPegawaiRepository->all();

        return view('diklat_pegawais.index')
            ->with('diklatPegawais', $diklatPegawais);
    }

    /**
     * Show the form for creating a new DiklatPegawai.
     *
     * @return Response
     */
    public function create()
    {
        return view('diklat_pegawais.create');
    }

    /**
     * Store a newly created DiklatPegawai in storage.
     *
     * @param CreateDiklatPegawaiRequest $request
     *
     * @return Response
     */
    public function store(CreateDiklatPegawaiRequest $request)
    {
        $input = $request->all();

        $diklatPegawai = $this->diklatPegawaiRepository->create($input);

        Flash::success('Diklat Pegawai saved successfully.');

        return redirect(route('diklatPegawais.index'));
    }

    /**
     * Display the specified DiklatPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $diklatPegawai = $this->diklatPegawaiRepository->find($id);

        if (empty($diklatPegawai)) {
            Flash::error('Diklat Pegawai not found');

            return redirect(route('diklatPegawais.index'));
        }

        return view('diklat_pegawais.show')->with('diklatPegawai', $diklatPegawai);
    }

    /**
     * Show the form for editing the specified DiklatPegawai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $diklatPegawai = $this->diklatPegawaiRepository->find($id);

        if (empty($diklatPegawai)) {
            Flash::error('Diklat Pegawai not found');

            return redirect(route('diklatPegawais.index'));
        }

        return view('diklat_pegawais.edit')->with('diklatPegawai', $diklatPegawai);
    }

    /**
     * Update the specified DiklatPegawai in storage.
     *
     * @param int $id
     * @param UpdateDiklatPegawaiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDiklatPegawaiRequest $request)
    {
        $diklatPegawai = $this->diklatPegawaiRepository->find($id);

        if (empty($diklatPegawai)) {
            Flash::error('Diklat Pegawai not found');

            return redirect(route('diklatPegawais.index'));
        }

        $diklatPegawai = $this->diklatPegawaiRepository->update($request->all(), $id);

        Flash::success('Diklat Pegawai updated successfully.');

        return redirect(route('diklatPegawais.index'));
    }

    /**
     * Remove the specified DiklatPegawai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $diklatPegawai = $this->diklatPegawaiRepository->find($id);

        if (empty($diklatPegawai)) {
            Flash::error('Diklat Pegawai not found');

            return redirect(route('diklatPegawais.index'));
        }

        $this->diklatPegawaiRepository->delete($id);

        Flash::success('Diklat Pegawai deleted successfully.');

        return redirect(route('diklatPegawais.index'));
    }
}
