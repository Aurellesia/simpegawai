<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJabatan_TambahanRequest;
use App\Http\Requests\UpdateJabatan_TambahanRequest;
use App\Repositories\Jabatan_TambahanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Jabatan_TambahanController extends AppBaseController
{
    /** @var  Jabatan_TambahanRepository */
    private $jabatanTambahanRepository;

    public function __construct(Jabatan_TambahanRepository $jabatanTambahanRepo)
    {
        $this->jabatanTambahanRepository = $jabatanTambahanRepo;
    }

    /**
     * Display a listing of the Jabatan_Tambahan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jabatanTambahans = $this->jabatanTambahanRepository->all();

        return view('jabatan__tambahans.index')
            ->with('jabatanTambahans', $jabatanTambahans);
    }

    /**
     * Show the form for creating a new Jabatan_Tambahan.
     *
     * @return Response
     */
    public function create()
    {
        return view('jabatan__tambahans.create');
    }

    /**
     * Store a newly created Jabatan_Tambahan in storage.
     *
     * @param CreateJabatan_TambahanRequest $request
     *
     * @return Response
     */
    public function store(CreateJabatan_TambahanRequest $request)
    {
        $input = $request->all();

        $jabatanTambahan = $this->jabatanTambahanRepository->create($input);

        Flash::success('Jabatan  Tambahan saved successfully.');

        return redirect(route('jabatanTambahans.index'));
    }

    /**
     * Display the specified Jabatan_Tambahan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jabatanTambahan = $this->jabatanTambahanRepository->find($id);

        if (empty($jabatanTambahan)) {
            Flash::error('Jabatan  Tambahan not found');

            return redirect(route('jabatanTambahans.index'));
        }

        return view('jabatan__tambahans.show')->with('jabatanTambahan', $jabatanTambahan);
    }

    /**
     * Show the form for editing the specified Jabatan_Tambahan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jabatanTambahan = $this->jabatanTambahanRepository->find($id);

        if (empty($jabatanTambahan)) {
            Flash::error('Jabatan  Tambahan not found');

            return redirect(route('jabatanTambahans.index'));
        }

        return view('jabatan__tambahans.edit')->with('jabatanTambahan', $jabatanTambahan);
    }

    /**
     * Update the specified Jabatan_Tambahan in storage.
     *
     * @param int $id
     * @param UpdateJabatan_TambahanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJabatan_TambahanRequest $request)
    {
        $jabatanTambahan = $this->jabatanTambahanRepository->find($id);

        if (empty($jabatanTambahan)) {
            Flash::error('Jabatan  Tambahan not found');

            return redirect(route('jabatanTambahans.index'));
        }

        $jabatanTambahan = $this->jabatanTambahanRepository->update($request->all(), $id);

        Flash::success('Jabatan  Tambahan updated successfully.');

        return redirect(route('jabatanTambahans.index'));
    }

    /**
     * Remove the specified Jabatan_Tambahan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jabatanTambahan = $this->jabatanTambahanRepository->find($id);

        if (empty($jabatanTambahan)) {
            Flash::error('Jabatan  Tambahan not found');

            return redirect(route('jabatanTambahans.index'));
        }

        $this->jabatanTambahanRepository->delete($id);

        Flash::success('Jabatan  Tambahan deleted successfully.');

        return redirect(route('jabatanTambahans.index'));
    }
}
