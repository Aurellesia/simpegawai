<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJabatan_StrukturalRequest;
use App\Http\Requests\UpdateJabatan_StrukturalRequest;
use App\Repositories\Jabatan_StrukturalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Jabatan_StrukturalController extends AppBaseController
{
    /** @var  Jabatan_StrukturalRepository */
    private $jabatanStrukturalRepository;

    public function __construct(Jabatan_StrukturalRepository $jabatanStrukturalRepo)
    {
        $this->jabatanStrukturalRepository = $jabatanStrukturalRepo;
    }

    /**
     * Display a listing of the Jabatan_Struktural.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jabatanStrukturals = $this->jabatanStrukturalRepository->all();

        return view('jabatan__strukturals.index')
            ->with('jabatanStrukturals', $jabatanStrukturals);
    }

    /**
     * Show the form for creating a new Jabatan_Struktural.
     *
     * @return Response
     */
    public function create()
    {
        return view('jabatan__strukturals.create');
    }

    /**
     * Store a newly created Jabatan_Struktural in storage.
     *
     * @param CreateJabatan_StrukturalRequest $request
     *
     * @return Response
     */
    public function store(CreateJabatan_StrukturalRequest $request)
    {
        $input = $request->all();

        $jabatanStruktural = $this->jabatanStrukturalRepository->create($input);

        Flash::success('Jabatan  Struktural saved successfully.');

        return redirect(route('jabatanStrukturals.index'));
    }

    /**
     * Display the specified Jabatan_Struktural.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jabatanStruktural = $this->jabatanStrukturalRepository->find($id);

        if (empty($jabatanStruktural)) {
            Flash::error('Jabatan  Struktural not found');

            return redirect(route('jabatanStrukturals.index'));
        }

        return view('jabatan__strukturals.show')->with('jabatanStruktural', $jabatanStruktural);
    }

    /**
     * Show the form for editing the specified Jabatan_Struktural.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jabatanStruktural = $this->jabatanStrukturalRepository->find($id);

        if (empty($jabatanStruktural)) {
            Flash::error('Jabatan  Struktural not found');

            return redirect(route('jabatanStrukturals.index'));
        }

        return view('jabatan__strukturals.edit')->with('jabatanStruktural', $jabatanStruktural);
    }

    /**
     * Update the specified Jabatan_Struktural in storage.
     *
     * @param int $id
     * @param UpdateJabatan_StrukturalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJabatan_StrukturalRequest $request)
    {
        $jabatanStruktural = $this->jabatanStrukturalRepository->find($id);

        if (empty($jabatanStruktural)) {
            Flash::error('Jabatan  Struktural not found');

            return redirect(route('jabatanStrukturals.index'));
        }

        $jabatanStruktural = $this->jabatanStrukturalRepository->update($request->all(), $id);

        Flash::success('Jabatan  Struktural updated successfully.');

        return redirect(route('jabatanStrukturals.index'));
    }

    /**
     * Remove the specified Jabatan_Struktural from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jabatanStruktural = $this->jabatanStrukturalRepository->find($id);

        if (empty($jabatanStruktural)) {
            Flash::error('Jabatan  Struktural not found');

            return redirect(route('jabatanStrukturals.index'));
        }

        $this->jabatanStrukturalRepository->delete($id);

        Flash::success('Jabatan  Struktural deleted successfully.');

        return redirect(route('jabatanStrukturals.index'));
    }
}
