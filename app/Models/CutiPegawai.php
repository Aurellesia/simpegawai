<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CutiPegawai extends Model
{
    use SoftDeletes;

    public $table = 'cuti_pegawais';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id_pegawai',
        'id_cuti',
        'tanggal_keluar',
        'jumlah_hari',
        'tanggal_masuk',
        'status'
    ];

    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'id_cuti' => 'integer'
    ];

    public static $rules = [
        'id_pegawai' => 'required',
        'id_cuti' => 'required',
    ];

    public function cuti(){
        return $this->hasOne(Cuti::class, 'id', 'id_cuti');
    }
}
