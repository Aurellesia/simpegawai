<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gapok
 * @package App\Models
 * @version March 18, 2020, 4:29 am UTC
 *
 * @property string masa_kerja
 * @property integer nominal_gaji
 */
class Gapok extends Model
{
    use SoftDeletes;

    public $table = 'gapoks';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'masa_kerja',
        'nominal_gaji'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'masa_kerja' => 'string',
        'nominal_gaji' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'masa_kerja' => 'required',
        'nominal_gaji' => 'required|numeric'
    ];

    
}
