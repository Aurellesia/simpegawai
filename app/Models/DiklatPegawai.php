<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DiklatPegawai
 * @package App\Models
 * @version April 3, 2020, 1:34 am UTC
 *
 * @property integer id_pegawai
 * @property integer id_diklat
 */
class DiklatPegawai extends Model
{
    use SoftDeletes;

    public $table = 'diklat_pegawais';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_pegawai',
        'id_diklat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'id_diklat' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pegawai' => 'required',
        'id_diklat' => 'required'
    ];
    public function diklat()
    {
            return $this->hasOne(Diklat::class, 'id', 'id_diklat');
    }


}
