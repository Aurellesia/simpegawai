<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class absensi
 * @package App\Models
 * @version March 23, 2020, 3:04 am UTC
 *
 * @property integer id_pegawai
 * @property string kehadiran
 * @property string tanggal
 * @property string keterangan
 */
class absensi extends Model
{
    use SoftDeletes;

    public $table = 'absensis';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_pegawai',
        'kehadiran',
        'tanggal',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'kehadiran' => 'string',
        'tanggal' => 'string',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pegawai' => 'required',
        // 'kehadiran' => 'required',
        'tanggal' => 'required'
    ];


}
