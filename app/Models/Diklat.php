<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Diklat
 * @package App\Models
 * @version March 18, 2020, 1:31 am UTC
 *
 * @property string jenis_diklat
 */
class Diklat extends Model
{
    use SoftDeletes;

    public $table = 'diklats';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'jenis_diklat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jenis_diklat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jenis_diklat' => 'required'
    ];

    
}
