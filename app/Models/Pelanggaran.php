<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pelanggaran
 * @package App\Models
 * @version March 23, 2020, 6:21 am UTC
 *
 * @property string pelanggaran
 */
class Pelanggaran extends Model
{
    use SoftDeletes;

    public $table = 'pelanggarans';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'pelanggaran'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pelanggaran' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pelanggaran' => 'required'
    ];

    
}
