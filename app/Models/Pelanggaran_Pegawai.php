<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pelanggaran_Pegawai
 * @package App\Models
 * @version March 23, 2020, 6:34 am UTC
 *
 * @property integer id_pegawai
 * @property integer pelanggaran
 * @property integer tindakan
 * @property string keterangan
 */
class Pelanggaran_Pegawai extends Model
{
    use SoftDeletes;

    public $table = 'pelanggaran__pegawais';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_pegawai',
        'pelanggaran',
        'tindakan',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'pelanggaran' => 'integer',
        'tindakan' => 'integer',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pegawai' => 'required',
        'pelanggaran' => 'required',
        'tindakan' => 'required'
    ];

    public function Pelanggaran()
    {
            return $this->hasOne(Pelanggaran::class, 'id', 'pelanggaran');
    }
    public function Tindakan()
    {
            return $this->hasOne(Tindakan::class, 'id', 'tindakan');
    }
}
