<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JabatanPegawai
 * @package App\Models
 * @version April 2, 2020, 1:15 pm UTC
 *
 * @property integer id_pegawai
 * @property integer id_struktural
 * @property integer id_fungsional
 * @property integer id_tambahan
 */
class JabatanPegawai extends Model
{
    use SoftDeletes;

    public $table = 'jabatan_pegawais';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_pegawai',
        'id_struktural',
        'id_fungsional',
        'id_tambahan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'id_struktural' => 'integer',
        'id_fungsional' => 'integer',
        'id_tambahan' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pegawai' => 'required',
        'id_struktural' => 'required',
        'id_fungsional' => 'required',
        'id_tambahan' => 'required'
    ];

    public function struktural()
    {
            return $this->hasOne(Jabatan_Struktural::class, 'id', 'id_struktural');
    }
    public function fungsional()
    {
            return $this->hasOne(Jabatan_Funsional::class, 'id', 'id_fungsional');
    }
    public function tambahan()
    {
            return $this->hasOne(Jabatan_Tambahan::class, 'id', 'id_tambahan');
    }
}
