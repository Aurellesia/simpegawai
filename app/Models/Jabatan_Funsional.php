<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Jabatan_Funsional
 * @package App\Models
 * @version March 18, 2020, 4:34 am UTC
 *
 * @property string jabatan_fungsional
 */
class Jabatan_Funsional extends Model
{
    use SoftDeletes;

    public $table = 'jabatan__funsionals';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'jabatan_fungsional'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jabatan_fungsional' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jabatan_fungsional' => 'required'
    ];

    
}
