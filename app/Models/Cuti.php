<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuti extends Model
{
    use SoftDeletes;

    public $table = 'cutis';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'jenis_cuti'
    ];

    protected $cats = [
        'id' => 'integer',
        'jenis_cuti' => 'string'
    ];

    public static $rules = [
        'jenis_cuti' => 'required'
    ];
}
