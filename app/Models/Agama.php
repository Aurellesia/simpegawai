<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Agama
 * @package App\Models
 * @version March 18, 2020, 1:11 am UTC
 *
 * @property string agama
 */
class Agama extends Model
{
    use SoftDeletes;

    public $table = 'agamas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'agama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'agama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'agama' => 'required'
    ];

    
}
