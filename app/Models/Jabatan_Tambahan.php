<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Jabatan_Tambahan
 * @package App\Models
 * @version March 18, 2020, 4:35 am UTC
 *
 * @property string jabatan_tambahan
 */
class Jabatan_Tambahan extends Model
{
    use SoftDeletes;

    public $table = 'jabatan__tambahans';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'jabatan_tambahan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jabatan_tambahan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jabatan_tambahan' => 'required'
    ];

    
}
