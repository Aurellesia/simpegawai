<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PenggajianPegawai
 * @package App\Models
 * @version March 30, 2020, 11:15 am UTC
 *
 * @property integer id_pegawai
 * @property integer gapok
 * @property integer gaji_tambahan
 * @property integer gaji_total
 */
class PenggajianPegawai extends Model
{
    use SoftDeletes;

    public $table = 'penggajian_pegawais';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_pegawai',
        'gapok',
        'gaji_tambahan',
        'gaji_total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pegawai' => 'integer',
        'gapok' => 'integer',
        'gaji_tambahan' => 'integer',
        'gaji_total' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pegawai' => 'required',
        'gapok' => 'required',
        'gaji_tambahan' => 'required|numeric'
    ];
    public function pelanggaran()
    {

    }
    public function Gapok()
    {

            return $this->hasOne(Gapok::class, 'id', 'gapok');
    }

}
