<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Jabatan_Struktural
 * @package App\Models
 * @version March 18, 2020, 4:32 am UTC
 *
 * @property string jabatan_struktural
 */
class Jabatan_Struktural extends Model
{
    use SoftDeletes;

    public $table = 'jabatan__strukturals';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'jabatan_struktural'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jabatan_struktural' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jabatan_struktural' => 'required'
    ];

    
}
