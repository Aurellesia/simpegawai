<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tindakan
 * @package App\Models
 * @version March 23, 2020, 6:22 am UTC
 *
 * @property string tindakan
 */
class Tindakan extends Model
{
    use SoftDeletes;

    public $table = 'tindakans';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tindakan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tindakan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tindakan' => 'required'
    ];

    
}
