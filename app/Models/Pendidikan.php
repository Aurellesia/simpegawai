<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pendidikan
 * @package App\Models
 * @version March 18, 2020, 4:37 am UTC
 *
 * @property string pendidikan
 */
class Pendidikan extends Model
{
    use SoftDeletes;

    public $table = 'pendidikans';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'pendidikan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pendidikan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pendidikan' => 'required'
    ];

    
}
