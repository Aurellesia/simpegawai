<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pegawai
 * @package App\Models
 * @version March 19, 2020, 5:53 am UTC
 *
 * @property string nip
 * @property string nama
 * @property string password
 * @property string tempat
 * @property string tgl_lahir
 * @property string telp
 * @property string jenis_kelamin
 * @property string status_pegawai
 * @property string foto
 * @property integer id_agama
 * @property string hobi
 * @property integer id_pendidikan
 * @property string nama_sekolah
 * @property string tahun_sttb
 * @property string gelar_depan
 * @property string gelar_belakang
 * @property string status_pernikahan
 */
class Pegawai extends Model
{
    use SoftDeletes;

    public $table = 'pegawais';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_user',
        'nip',
        'nama',
        'password',
        'tempat',
        'tgl_lahir',
        'telp',
        'jenis_kelamin',
        'status_pegawai',
        'foto',
        'id_agama',
        'hobi',
        'id_pendidikan',
        'nama_sekolah',
        'tahun_sttb',
        'gelar_depan',
        'gelar_belakang',
        'status_pernikahan',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nip' => 'string',
        'nama' => 'string',
        'password' => 'string',
        'tempat' => 'string',
        'tgl_lahir' => 'string',
        'telp' => 'string',
        'jenis_kelamin' => 'string',
        'status_pegawai' => 'string',
        'foto' => 'string',
        'id_agama' => 'integer',
        'hobi' => 'string',
        'id_pendidikan' => 'integer',
        'nama_sekolah' => 'string',
        'tahun_sttb' => 'string',
        'gelar_depan' => 'string',
        'gelar_belakang' => 'string',
        'status_pernikahan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nip' => 'required:numeric',
        'nama' => 'required',
        'password' => 'required',
        'tempat' => 'required',
        'tgl_lahir' => 'required',
        'telp' => 'required:numeric',
        'jenis_kelamin' => 'required',
        'status_pegawai' => 'required',
        'foto' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        'id_agama' => 'required',
        'hobi' => 'required',
        'id_pendidikan' => 'required',
        'nama_sekolah' => 'required',
        'tahun_sttb' => 'required',
        'gelar_depan' => 'required',
        'gelar_belakang' => 'required',
        'status_pernikahan' => 'required'
    ];
    public function agama()
    {
        return $this->hasOne(Agama::class, 'id', 'id_agama');
    }
    public function pendidikan()
    {
        return $this->hasOne(Pendidikan::class, 'id', 'id_pendidikan');
    }
    public function diklat()
    {
            return $this->hasOne(Diklat::class, 'id', 'id_diklat');
    }
    public function struktural()
    {
            return $this->hasOne(Jabatan_Struktural::class, 'id', 'id_struktural');
    }
    public function fungsional()
    {
            return $this->hasOne(Jabatan_Funsional::class, 'id', 'id_fungsional');
    }
    public function tambahan()
    {
        return $this->hasOne(Jabatan_Tambahan::class, 'id', 'id_tambahan');
    }
    public function Gapok()
    {
            return $this->hasOne(Gapok::class, 'id', 'gapok');
    }
    public function pelanggaran()
    {
            return $this->hasOne(Pelanggaran::class, 'id', 'pelanggaran');
    }
    public function tindakan()
    {
            return $this->hasOne(Tindakan::class, 'id', 'tindakan');
    }

    public function users(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }


}
