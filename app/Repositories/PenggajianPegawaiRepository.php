<?php

namespace App\Repositories;

use App\Models\PenggajianPegawai;
use App\Repositories\BaseRepository;

/**
 * Class PenggajianPegawaiRepository
 * @package App\Repositories
 * @version March 30, 2020, 11:15 am UTC
*/

class PenggajianPegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'gapok',
        'gaji_tambahan',
        'gaji_total'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PenggajianPegawai::class;
    }
}
