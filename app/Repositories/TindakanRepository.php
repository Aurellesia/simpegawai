<?php

namespace App\Repositories;

use App\Models\Tindakan;
use App\Repositories\BaseRepository;

/**
 * Class TindakanRepository
 * @package App\Repositories
 * @version March 23, 2020, 6:22 am UTC
*/

class TindakanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tindakan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tindakan::class;
    }
}
