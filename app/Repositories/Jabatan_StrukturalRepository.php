<?php

namespace App\Repositories;

use App\Models\Jabatan_Struktural;
use App\Repositories\BaseRepository;

/**
 * Class Jabatan_StrukturalRepository
 * @package App\Repositories
 * @version March 18, 2020, 4:32 am UTC
*/

class Jabatan_StrukturalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jabatan_struktural'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jabatan_Struktural::class;
    }
}
