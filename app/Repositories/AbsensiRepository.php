<?php

namespace App\Repositories;

use App\Models\absensi;
use App\Repositories\BaseRepository;

/**
 * Class absensiRepository
 * @package App\Repositories
 * @version March 23, 2020, 3:04 am UTC
*/

class absensiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'kehadiran',
        'tanggal',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return absensi::class;
    }
}
