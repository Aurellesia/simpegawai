<?php

namespace App\Repositories;

use App\Models\DiklatPegawai;
use App\Repositories\BaseRepository;

/**
 * Class DiklatPegawaiRepository
 * @package App\Repositories
 * @version April 3, 2020, 1:34 am UTC
*/

class DiklatPegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'id_diklat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DiklatPegawai::class;
    }
}
