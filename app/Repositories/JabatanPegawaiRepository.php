<?php

namespace App\Repositories;

use App\Models\JabatanPegawai;
use App\Repositories\BaseRepository;

/**
 * Class JabatanPegawaiRepository
 * @package App\Repositories
 * @version April 2, 2020, 1:15 pm UTC
*/

class JabatanPegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'id_struktural',
        'id_fungsional',
        'id_tambahan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JabatanPegawai::class;
    }
}
