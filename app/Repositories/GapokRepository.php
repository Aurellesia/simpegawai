<?php

namespace App\Repositories;

use App\Models\Gapok;
use App\Repositories\BaseRepository;

/**
 * Class GapokRepository
 * @package App\Repositories
 * @version March 18, 2020, 4:29 am UTC
*/

class GapokRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'masa_kerja',
        'nominal_gaji'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gapok::class;
    }
}
