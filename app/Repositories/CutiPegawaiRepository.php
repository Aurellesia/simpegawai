<?php

namespace App\Repositories;

use App\Models\CutiPegawai;
use App\Repositories\BaseRepository;

class CutiPegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'id_cuti'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CutiPegawai::class;
    }
}
