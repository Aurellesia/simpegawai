<?php

namespace App\Repositories;

use App\Models\Pelanggaran_Pegawai;
use App\Repositories\BaseRepository;

/**
 * Class Pelanggaran_PegawaiRepository
 * @package App\Repositories
 * @version March 23, 2020, 6:34 am UTC
*/

class Pelanggaran_PegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pegawai',
        'pelanggaran',
        'tindakan',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pelanggaran_Pegawai::class;
    }
}
