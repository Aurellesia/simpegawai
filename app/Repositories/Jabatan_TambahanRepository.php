<?php

namespace App\Repositories;

use App\Models\Jabatan_Tambahan;
use App\Repositories\BaseRepository;

/**
 * Class Jabatan_TambahanRepository
 * @package App\Repositories
 * @version March 18, 2020, 4:35 am UTC
*/

class Jabatan_TambahanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jabatan_tambahan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jabatan_Tambahan::class;
    }
}
