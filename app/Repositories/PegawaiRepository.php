<?php

namespace App\Repositories;

use App\Models\Pegawai;
use App\Repositories\BaseRepository;

/**
 * Class PegawaiRepository
 * @package App\Repositories
 * @version March 19, 2020, 5:53 am UTC
*/

class PegawaiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'nip',
        'nama',
        'password',
        'tempat',
        'tgl_lahir',
        'telp',
        'jenis_kelamin',
        'status_pegawai',
        'foto',
        'id_agama',
        'hobi',
        'id_pendidikan',
        'nama_sekolah',
        'tahun_sttb',
        'gelar_depan',
        'gelar_belakang',
        'status_pernikahan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pegawai::class;
    }
}
