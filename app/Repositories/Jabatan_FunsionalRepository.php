<?php

namespace App\Repositories;

use App\Models\Jabatan_Funsional;
use App\Repositories\BaseRepository;

/**
 * Class Jabatan_FunsionalRepository
 * @package App\Repositories
 * @version March 18, 2020, 4:34 am UTC
*/

class Jabatan_FunsionalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jabatan_fungsional'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jabatan_Funsional::class;
    }
}
