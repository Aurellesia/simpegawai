<?php

namespace App\Repositories;

use App\Models\Pelanggaran;
use App\Repositories\BaseRepository;

/**
 * Class PelanggaranRepository
 * @package App\Repositories
 * @version March 23, 2020, 6:21 am UTC
*/

class PelanggaranRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pelanggaran'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pelanggaran::class;
    }
}
