<?php

namespace App\Repositories;

use App\Models\Diklat;
use App\Repositories\BaseRepository;

/**
 * Class DiklatRepository
 * @package App\Repositories
 * @version March 18, 2020, 1:31 am UTC
*/

class DiklatRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis_diklat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Diklat::class;
    }
}
