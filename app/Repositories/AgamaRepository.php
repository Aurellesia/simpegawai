<?php

namespace App\Repositories;

use App\Models\Agama;
use App\Repositories\BaseRepository;

/**
 * Class AgamaRepository
 * @package App\Repositories
 * @version March 18, 2020, 1:11 am UTC
*/

class AgamaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agama'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agama::class;
    }
}
