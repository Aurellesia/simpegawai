<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiklatPegawaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diklat_pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pegawai')->unsigned();
            $table->integer('id_diklat')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_pegawai')->references('id')->on('pegawais');
            $table->foreign('id_diklat')->references('id')->on('diklats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diklat_pegawais');
    }
}
