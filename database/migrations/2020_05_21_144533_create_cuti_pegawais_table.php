<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiPegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti_pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pegawai')->unsigned();
            $table->integer('id_cuti')->unsigned();
            $table->string('tanggal_keluar');
            $table->integer('jumlah_hari');
            $table->string('tanggal_masuk');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_pegawai')->references('id')->on('pegawais');
            $table->foreign('id_cuti')->references('id')->on('cutis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuti_pegawais');
    }
}
