<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePenggajianPegawaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggajian_pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pegawai')->unsigned();
            $table->integer('gapok')->unsigned();
            $table->integer('gaji_tambahan');
            $table->integer('gaji_total');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_pegawai')->references('id')->on('pegawais');
            $table->foreign('gapok')->references('id')->on('pegawais');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penggajian_pegawais');
    }
}
