<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePelanggaranPegawaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggaran__pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pegawai')->unsigned();
            $table->integer('pelanggaran')->unsigned();
            $table->integer('tindakan')->unsigned();
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_pegawai')->references('id')->on('pegawais');
            $table->foreign('pelanggaran')->references('id')->on('pelanggarans');
            $table->foreign('tindakan')->references('id')->on('tindakans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pelanggaran__pegawais');
    }
}
