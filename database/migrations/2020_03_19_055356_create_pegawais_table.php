<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePegawaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('nip');
            $table->string('nama');
            $table->string('password');
            $table->string('tempat');
            $table->string('tgl_lahir');
            $table->string('telp');
            $table->string('jenis_kelamin');
            $table->string('status_pegawai');
            $table->string('foto');
            $table->integer('id_agama')->unsigned();
            $table->string('hobi');
            $table->integer('id_pendidikan')->unsigned();
            $table->string('nama_sekolah');
            $table->string('tahun_sttb');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('status_pernikahan');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_agama')->references('id')->on('agamas');
            $table->foreign('id_pendidikan')->references('id')->on('pendidikans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pegawais');
    }
}
