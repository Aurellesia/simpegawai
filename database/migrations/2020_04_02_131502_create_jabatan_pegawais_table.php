<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJabatanPegawaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_pegawais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pegawai')->unsigned();
            $table->integer('id_struktural')->unsigned();
            $table->integer('id_fungsional')->unsigned();
            $table->integer('id_tambahan')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_pegawai')->references('id')->on('pegawais');
            $table->foreign('id_struktural')->references('id')->on('jabatan__strukturals');
            $table->foreign('id_fungsional')->references('id')->on('jabatan__funsionals');
            $table->foreign('id_tambahan')->references('id')->on('jabatan__tambahans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jabatan_pegawais');
    }
}
