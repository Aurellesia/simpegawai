<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jabatan_Tambahan;
use Faker\Generator as Faker;

$factory->define(Jabatan_Tambahan::class, function (Faker $faker) {

    return [
        'jabatan_tambahan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
