<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pelanggaran_Pegawai;
use Faker\Generator as Faker;

$factory->define(Pelanggaran_Pegawai::class, function (Faker $faker) {

    return [
        'id_pegawai' => $faker->randomDigitNotNull,
        'pelanggaran' => $faker->randomDigitNotNull,
        'tindakan' => $faker->randomDigitNotNull,
        'keterangan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
