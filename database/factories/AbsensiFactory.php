<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Absensi;
use Faker\Generator as Faker;

$factory->define(Absensi::class, function (Faker $faker) {

    return [
        'id_pegawai' => $faker->randomDigitNotNull,
        'kehadiran' => $faker->word,
        'tanggal' => $faker->word,
        'keterangan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
