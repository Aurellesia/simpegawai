<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jabatan_Funsional;
use Faker\Generator as Faker;

$factory->define(Jabatan_Funsional::class, function (Faker $faker) {

    return [
        'jabatan_fungsional' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
