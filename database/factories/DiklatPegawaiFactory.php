<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DiklatPegawai;
use Faker\Generator as Faker;

$factory->define(DiklatPegawai::class, function (Faker $faker) {

    return [
        'id_pegawai' => $faker->randomDigitNotNull,
        'id_diklat' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
