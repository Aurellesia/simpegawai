<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PenggajianPegawai;
use Faker\Generator as Faker;

$factory->define(PenggajianPegawai::class, function (Faker $faker) {

    return [
        'id_pegawai' => $faker->randomDigitNotNull,
        'gapok' => $faker->randomDigitNotNull,
        'gaji_tambahan' => $faker->randomDigitNotNull,
        'gaji_total' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
