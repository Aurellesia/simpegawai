<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jabatan_Struktural;
use Faker\Generator as Faker;

$factory->define(Jabatan_Struktural::class, function (Faker $faker) {

    return [
        'jabatan_struktural' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
