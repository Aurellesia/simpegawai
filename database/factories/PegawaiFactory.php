<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pegawai;
use Faker\Generator as Faker;

$factory->define(Pegawai::class, function (Faker $faker) {

    return [
        'nip' => $faker->word,
        'nama' => $faker->word,
        'password' => $faker->word,
        'tempat' => $faker->word,
        'tgl_lahir' => $faker->word,
        'telp' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'status_pegawai' => $faker->word,
        'foto' => $faker->word,
        'id_agama' => $faker->randomDigitNotNull,
        'hobi' => $faker->word,
        'id_pendidikan' => $faker->randomDigitNotNull,
        'nama_sekolah' => $faker->word,
        'tahun_sttb' => $faker->word,
        'gelar_depan' => $faker->word,
        'gelar_belakang' => $faker->word,
        'status_pernikahan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
