<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\JabatanPegawai;
use Faker\Generator as Faker;

$factory->define(JabatanPegawai::class, function (Faker $faker) {

    return [
        'id_pegawai' => $faker->randomDigitNotNull,
        'id_struktural' => $faker->randomDigitNotNull,
        'id_fungsional' => $faker->randomDigitNotNull,
        'id_tambahan' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
