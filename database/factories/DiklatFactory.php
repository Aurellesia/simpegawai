<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Diklat;
use Faker\Generator as Faker;

$factory->define(Diklat::class, function (Faker $faker) {

    return [
        'jenis_diklat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
