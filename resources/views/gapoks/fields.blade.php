<!-- Masa Kerja Field -->
<div class="form-group col-sm-6">
    {!! Form::label('masa_kerja', 'Masa Kerja:') !!}
    {!! Form::text('masa_kerja', null, ['class' => 'form-control']) !!}
</div>

<!-- Nominal Gaji Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominal_gaji', 'Nominal Gaji:') !!}
    {!! Form::number('nominal_gaji', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('gapoks.index') }}" class="btn btn-secondary">Cancel</a>
</div>
