<div class="table-responsive-sm">
    <table class="table table-striped" id="gapoks-table">
        <thead>
            <tr>
                <th>Masa Kerja</th>
        <th>Nominal Gaji</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($gapoks as $gapok)
            <tr>
                <td>{{ $gapok->masa_kerja }}</td>
            <td>{{ $gapok->nominal_gaji }}</td>
                <td>
                    {!! Form::open(['route' => ['gapoks.destroy', $gapok->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('gapoks.show', [$gapok->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('gapoks.edit', [$gapok->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>