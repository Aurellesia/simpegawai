<!-- Masa Kerja Field -->
<div class="form-group">
    {!! Form::label('masa_kerja', 'Masa Kerja:') !!}
    <p>{{ $gapok->masa_kerja }}</p>
</div>

<!-- Nominal Gaji Field -->
<div class="form-group">
    {!! Form::label('nominal_gaji', 'Nominal Gaji:') !!}
    <p>{{ $gapok->nominal_gaji }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $gapok->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $gapok->updated_at }}</p>
</div>

