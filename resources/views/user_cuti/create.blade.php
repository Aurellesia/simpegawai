@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('user_cuti.index') !!}">Cuti</a>
      </li>
      <li class="breadcrumb-item active">Buat Permohonan Cuti</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Cuti</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'user_cuti.store']) !!}

                                   @include('user_cuti.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
