<!-- Jenis Cuti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_cuti', 'Jenis Cuti:') !!}
    <p>{{ $cutiPegawais->cuti->jenis_cuti }}</p>
</div>

<!-- tanggal keluar field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_keluar', 'Tanggal Keluar:') !!}
    <p>{{ $cutiPegawais->tanggal_keluar }}</p>
</div>

<!-- jumlah hari field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah_hari', 'Jumlah Hari:') !!}
    <p>{{ $cutiPegawais->jumlah_hari }}</p>
</div>

<!-- masuk tanggal -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_masuk', 'Tanggal Masuk:') !!}
    <p>{{ $cutiPegawais->tanggal_masuk }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $cutiPegawais->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $cutiPegawais->updated_at }}</p>
</div>

