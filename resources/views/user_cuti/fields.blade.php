<div class="form-group col-sm-6">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    {!! Form::text('id_pegawai', $pegawai) !!}
</div>

<!-- Jenis Cuti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_cuti', 'Cuti:') !!}
    {!! Form::select('id_cuti', $cuti, null, ['class' => 'form-control']) !!}
</div>

<!-- Dari tanggal field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_keluar', 'Tanggal Keluar:') !!}
    {!! Form::date('tanggal_keluar', null, ['class' => 'form-control']) !!}
</div>

<!-- jumlah hari field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah_hari', 'Jumlah Hari:') !!}
    {!! Form::number('jumlah_hari', null, ['class' => 'form-control']) !!}
</div>

<!-- tanggal masuk -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal_masuk', 'Tanggal Masuk:') !!}
    {!! Form::date('tanggal_masuk', null, ['class' => 'form-control']) !!}
</div>

<!-- status -->
<div class="form-group col-sm-6">
    {!! Form::hidden('status','Belum Disetujui', ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('user_cuti.index') }}" class="btn btn-secondary">Cancel</a>
</div>
