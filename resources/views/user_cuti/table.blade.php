<div class="table-responsive-sm">
    <table class="table table-striped" id="cuti-table">
        <thead>
            <tr>
                <th>Jenis Cuti</th>
                <th>Tanggal Keluar</th>
                <th>Jumlah Hari</th>
                <th>Tanggal Masuk</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cutiPegawais as $cutiPegawai)
            <tr>
                <td>{{ $cutiPegawai->cuti->jenis_cuti }}</td>
                <td>{{ date('d-m-Y', strtotime($cutiPegawai->tanggal_keluar)) }}</td>
                <td>{{ $cutiPegawai->jumlah_hari }}</td>
                <td>{{ date('d-m-Y', strtotime($cutiPegawai->tanggal_masuk)) }}</td>
                <td>{{ $cutiPegawai->status }}</td>
                <td>
                    {!! Form::open(['route' => ['user_cuti.destroy', $cutiPegawai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('user_cuti.show', [$cutiPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('user_cuti.edit', [$cutiPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>