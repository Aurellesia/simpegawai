<div class="table-responsive-sm">
    <table class="table table-striped" id="pelanggaran-table">
        <thead>
            <tr>
                <th>Pelanggaran</th>
                <th>Tindakan</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pelanggaranPegawais as $pelanggaranPegawai)
            <tr>
                <td>{{ $pelanggaranPegawai->Pelanggaran->pelanggaran }}</td>
                <td>{{ $pelanggaranPegawai->Tindakan->tindakan }}</td>
                <td>{{ $pelanggaranPegawai->keterangan }}</td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>