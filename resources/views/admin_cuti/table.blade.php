<div class="table-responsive-sm">
    <table class="table table-striped" id="cuti-table">
        <thead>
            <tr>
                <th>Nama Pegawai</th>
                <th>Jenis Cuti</th>
                <th>Tanggal Keluar</th>
                <th>Jumlah Hari</th>
                <th>Tanggal Masuk</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $cutiPegawai)
            <tr>
                <td>{{ $cutiPegawai->id_pegawai }}</td>
                <td>{{ $cutiPegawai->cuti->jenis_cuti }}</td>
                <td>{{ date('d-m-Y', strtotime($cutiPegawai->tanggal_keluar)) }}</td>
                <td>{{ $cutiPegawai->jumlah_hari }}</td>
                <td>{{ date('d-m-Y', strtotime($cutiPegawai->tanggal_masuk)) }}</td>
                <td>{{ $cutiPegawai->status }}</td>
                <td>
                    <form action="/admin/cuti/acc" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $cutiPegawai->id }}" name="id">
                        <input type="submit" value="Terima" class="btn btn-ghost-primary">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
