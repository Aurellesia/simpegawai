<div class="table-responsive-sm">
    <table class="table table-striped" id="gaji-table">
        <thead>
            <tr>
                <th>Gaji Pokok</th>
                <th>Gaji Tambahan</th>
                <th>Total Diterima</th>
            </tr>
        </thead>
        <tbody>
            @foreach($gajiPegawais as $gajiPegawai)
            <tr>
                <td>{{ $gajiPegawai->Gapok->nominal_gaji }}</td>
                <td>{{ $gajiPegawai->gaji_tambahan }}</td>
                <td>{{ $gajiPegawai->gaji_total }}</td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>