<div class="table-responsive-sm">
    <table class="table table-striped" id="diklat-table">
        <thead>
            <tr>
                <th>Diklat</th>
            </tr>
        </thead>
        <tbody>
            @foreach($diklatPegawais as $diklatPegawai)
            <tr>
                <td>{{ $diklatPegawai->diklat->jenis_diklat }}</td>

            </tr>
            @endforeach

        </tbody>
    </table>
</div>