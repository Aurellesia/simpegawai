<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatanTambahans-table">
        <thead>
            <tr>
                <th>Jabatan Tambahan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jabatanTambahans as $jabatanTambahan)
            <tr>
                <td>{{ $jabatanTambahan->jabatan_tambahan }}</td>
                <td>
                    {!! Form::open(['route' => ['jabatanTambahans.destroy', $jabatanTambahan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('jabatanTambahans.show', [$jabatanTambahan->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jabatanTambahans.edit', [$jabatanTambahan->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>