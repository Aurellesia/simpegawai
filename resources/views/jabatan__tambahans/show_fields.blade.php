<!-- Jabatan Tambahan Field -->
<div class="form-group">
    {!! Form::label('jabatan_tambahan', 'Jabatan Tambahan:') !!}
    <p>{{ $jabatanTambahan->jabatan_tambahan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $jabatanTambahan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $jabatanTambahan->updated_at }}</p>
</div>

