<!-- Jabatan Tambahan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jabatan_tambahan', 'Jabatan Tambahan:') !!}
    {!! Form::text('jabatan_tambahan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jabatanTambahans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
