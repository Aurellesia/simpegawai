@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Absensis</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Absensis
                             {{--  {!! Form::open(['route' => 'absensis.index']) !!}
                             <div class="text-center" method="get" action="absensis.index">
                                @csrf
                                <select name="tanggal"class="form-control col-sm-2 d-inline" id="">
                                    <option disabled value="">pilih bulan</option>
                                    @php $i=1; @endphp
                                    @foreach ($tanggal as $tgl)
                                    <option value="{{$i}}">{{$tgl}}</option>
                                    @php $i++; @endphp
                                    @endforeach
                                </select>
                                <label for="tahun">Tahun : </label>
                                <input type="number" class="form-control col-sm-2 d-inline" id="tahun">
                                <button type="submit" class="btn btn-primary">Cari</button>
                             </div>
                             {!! Form::close() !!}  --}}
                             <a class="pull-right" href="{{ route('absensis.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('absensis.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

