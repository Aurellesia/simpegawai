<div class="table-responsive-sm">
    <table class="table table-striped" id="absensis-table">
        <thead>
            <tr>
                <th>Pegawai</th>
                @foreach($pegawai as $pg)
                    @foreach ($absensis as $item)
                        @if($pg->id == $item->id_pegawai)
                            @php
                             $tgl=explode("/",$item->tanggal);
                            @endphp
                            <th>{{$tgl[0]}}</th>
                        @endif
                    @endforeach
                    @break
                @endforeach
            </tr>
        </thead>
        <tbody>
        @foreach($pegawai as $pg)
            <tr>
                <td>{{$pg->nama}}</td>
                @foreach ($absensis as $item)
                    @if($pg->id == $item->id_pegawai)
                        <td>{{ $item->kehadiran }}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
