<table class="table table-striped" style="text-align:center;" id="absensis-table">
    <thead>
        <tr>
            <th rowspan="2">Id Pegawai</th>
            <th colspan="3">Kehadiran</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>Masuk</th>
            <th>izin</th>
            <th>tidak masuk</th>
        </tr>
    </thead>
    <tbody>
    @foreach($pegawai as $p)
        <tr>
            <td>
                @php
                $mytime = date('d/m/Y');
                @endphp
                <input type="hidden" name="id_pegawai[]" value="{{$p->id}}">
                <input type="hidden" name="tanggal[]" value="{{$mytime}}">
                {{$p->nama}}

            </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[{{$p->id}}]" checked="checked" value="Masuk">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[{{$p->id}}]" value="Izin">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input class="form-check-input" type="radio" name="kehadiran[{{$p->id}}]" value="tidak masuk">
            </label>
        </td>
        <td>
            <!-- Keterangan Field -->
            <div class="form-group col-sm-12">
                {!! Form::text('keterangan[]', null, ['class' => 'form-control']) !!}
            </div>
        </td>
        </tr>
    @endforeach
    </tbody>
</table>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('absensis.index') }}" class="btn btn-secondary">Cancel</a>
</div>
