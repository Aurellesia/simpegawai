<!-- Id Pegawai Field -->
<div class="form-group">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    <p>{{ $absensi->id_pegawai }}</p>
</div>

<!-- Kehadiran Field -->
<div class="form-group">
    {!! Form::label('kehadiran', 'Kehadiran:') !!}
    <p>{{ $absensi->kehadiran }}</p>
</div>

<!-- Tanggal Field -->
<div class="form-group">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $absensi->tanggal }}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{{ $absensi->keterangan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $absensi->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $absensi->updated_at }}</p>
</div>

