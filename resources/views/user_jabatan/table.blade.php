<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatan-table">
        <thead>
            <tr>
                <th>Jabatan Struktural</th>
                <th>Jabatan Fungsional</th>
                <th>Jabatan Tambahan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jabatanPegawais as $jabatanPegawai)
            <tr>
                <td>{{ $jabatanPegawai->struktural->jabatan_struktural }}</td>
                <td>{{ $jabatanPegawai->fungsional->jabatan_fungsional }}</td>
                <td>{{ $jabatanPegawai->tambahan->jabatan_tambahan }}</td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>