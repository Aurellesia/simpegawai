<div class="table-responsive-sm">
    <table class="table table-striped" id="pelanggaranPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
        <th>Pelanggaran</th>
        <th>Tindakan</th>
        <th>Keterangan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pelanggaranPegawais as $pelanggaranPegawai)
            <tr>
            <td>{{ $pelanggaranPegawai->id_pegawai }}</td>
            <td>{{ $pelanggaranPegawai->Pelanggaran->pelanggaran }}</td>
            <td>{{ $pelanggaranPegawai->Tindakan->tindakan }}</td>
            <td>{{ $pelanggaranPegawai->keterangan }}</td>
                <td>
                    {!! Form::open(['route' => ['pelanggaranPegawais.destroy', $pelanggaranPegawai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pelanggaranPegawais.show', [$pelanggaranPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('pelanggaranPegawais.edit', [$pelanggaranPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
