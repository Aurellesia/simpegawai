<!-- Id Pegawai Field -->
<div class="form-group">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    <p>{{ $pelanggaranPegawai->id_pegawai }}</p>
</div>

<!-- Pelanggaran Field -->
<div class="form-group">
    {!! Form::label('pelanggaran', 'Pelanggaran:') !!}
    <p>{{ $pelanggaranPegawai->pelanggaran }}</p>
</div>

<!-- Tindakan Field -->
<div class="form-group">
    {!! Form::label('tindakan', 'Tindakan:') !!}
    <p>{{ $pelanggaranPegawai->tindakan }}</p>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{{ $pelanggaranPegawai->keterangan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $pelanggaranPegawai->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $pelanggaranPegawai->updated_at }}</p>
</div>

