<!-- Id Pegawai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    {!! Form::select('id_pegawai', $pegawaiItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Pelanggaran Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pelanggaran', 'Pelanggaran:') !!}
    {!! Form::select('pelanggaran', $pelanggaranItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Tindakan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tindakan', 'Tindakan:') !!}
    {!! Form::select('tindakan', $tindakanItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Keterangan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pelanggaranPegawais.index') }}" class="btn btn-secondary">Cancel</a>
</div>
