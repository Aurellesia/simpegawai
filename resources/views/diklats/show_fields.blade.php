<!-- Jenis Diklat Field -->
<div class="form-group">
    {!! Form::label('jenis_diklat', 'Jenis Diklat:') !!}
    <p>{{ $diklat->jenis_diklat }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $diklat->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $diklat->updated_at }}</p>
</div>

