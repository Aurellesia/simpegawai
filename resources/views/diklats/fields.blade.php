<!-- Jenis Diklat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_diklat', 'Jenis Diklat:') !!}
    {!! Form::text('jenis_diklat', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('diklats.index') }}" class="btn btn-secondary">Cancel</a>
</div>
