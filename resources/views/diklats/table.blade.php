<div class="table-responsive-sm">
    <table class="table table-striped" id="diklats-table">
        <thead>
            <tr>
                <th>Jenis Diklat</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($diklats as $diklat)
            <tr>
                <td>{{ $diklat->jenis_diklat }}</td>
                <td>
                    {!! Form::open(['route' => ['diklats.destroy', $diklat->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('diklats.show', [$diklat->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('diklats.edit', [$diklat->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>