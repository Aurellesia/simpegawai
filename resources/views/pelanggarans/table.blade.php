<div class="table-responsive-sm">
    <table class="table table-striped" id="pelanggarans-table">
        <thead>
            <tr>
                <th>Pelanggaran</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pelanggarans as $pelanggaran)
            <tr>
                <td>{{ $pelanggaran->pelanggaran }}</td>
                <td>
                    {!! Form::open(['route' => ['pelanggarans.destroy', $pelanggaran->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pelanggarans.show', [$pelanggaran->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('pelanggarans.edit', [$pelanggaran->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>