<!-- Pelanggaran Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pelanggaran', 'Pelanggaran:') !!}
    {!! Form::text('pelanggaran', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pelanggarans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
