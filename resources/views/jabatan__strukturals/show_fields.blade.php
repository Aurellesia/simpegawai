<!-- Jabatan Struktural Field -->
<div class="form-group">
    {!! Form::label('jabatan_struktural', 'Jabatan Struktural:') !!}
    <p>{{ $jabatanStruktural->jabatan_struktural }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $jabatanStruktural->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $jabatanStruktural->updated_at }}</p>
</div>

