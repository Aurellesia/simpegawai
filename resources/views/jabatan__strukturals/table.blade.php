<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatanStrukturals-table">
        <thead>
            <tr>
                <th>Jabatan Struktural</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jabatanStrukturals as $jabatanStruktural)
            <tr>
                <td>{{ $jabatanStruktural->jabatan_struktural }}</td>
                <td>
                    {!! Form::open(['route' => ['jabatanStrukturals.destroy', $jabatanStruktural->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('jabatanStrukturals.show', [$jabatanStruktural->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jabatanStrukturals.edit', [$jabatanStruktural->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>