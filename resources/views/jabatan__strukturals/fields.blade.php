<!-- Jabatan Struktural Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jabatan_struktural', 'Jabatan Struktural:') !!}
    {!! Form::text('jabatan_struktural', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jabatanStrukturals.index') }}" class="btn btn-secondary">Cancel</a>
</div>
