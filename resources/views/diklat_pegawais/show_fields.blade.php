<!-- Id Pegawai Field -->
<div class="form-group">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    <p>{{ $diklatPegawai->id_pegawai }}</p>
</div>

<!-- Id Diklat Field -->
<div class="form-group">
    {!! Form::label('id_diklat', 'Id Diklat:') !!}
    <p>{{ $diklatPegawai->id_diklat }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $diklatPegawai->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $diklatPegawai->updated_at }}</p>
</div>

