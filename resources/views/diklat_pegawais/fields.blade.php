<!-- Id Pegawai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    {!! Form::select('id_pegawai', $pegawaiItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Id Diklat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_diklat', 'Id Diklat:') !!}
    {!! Form::select('id_diklat', $diklatItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('diklatPegawais.index') }}" class="btn btn-secondary">Cancel</a>
</div>
