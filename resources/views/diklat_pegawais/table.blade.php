<div class="table-responsive-sm">
    <table class="table table-striped" id="diklatPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
        <th>Id Diklat</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($diklatPegawais as $diklatPegawai)
            <tr>
                <td>{{ $diklatPegawai->id_pegawai }}</td>
                <td>{{ $diklatPegawai->diklat->jenis_diklat }}</td>
                <td>
                    {!! Form::open(['route' => ['diklatPegawais.destroy', $diklatPegawai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('diklatPegawais.show', [$diklatPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('diklatPegawais.edit', [$diklatPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
