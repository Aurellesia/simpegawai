<!-- Jenis Cuti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_cuti', 'Jenis Cuti:') !!}
    {!! Form::text('jenis_cuti', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cutis.index') }}" class="btn btn-secondary">Cancel</a>
</div>
