<!-- Jenis Cuti Field -->
<div class="form-group">
    {!! Form::label('jenis_cuti', 'Jenis Cuti:') !!}
    <p>{{ $cuti->jenis_cuti }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $cuti->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $cuti->updated_at }}</p>
</div>

