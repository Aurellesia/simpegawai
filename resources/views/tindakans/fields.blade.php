<!-- Tindakan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tindakan', 'Tindakan:') !!}
    {!! Form::text('tindakan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tindakans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
