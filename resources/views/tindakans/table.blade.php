<div class="table-responsive-sm">
    <table class="table table-striped" id="tindakans-table">
        <thead>
            <tr>
                <th>Tindakan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tindakans as $tindakan)
            <tr>
                <td>{{ $tindakan->tindakan }}</td>
                <td>
                    {!! Form::open(['route' => ['tindakans.destroy', $tindakan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('tindakans.show', [$tindakan->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('tindakans.edit', [$tindakan->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>