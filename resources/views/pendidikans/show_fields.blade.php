<!-- Pendidikan Field -->
<div class="form-group">
    {!! Form::label('pendidikan', 'Pendidikan:') !!}
    <p>{{ $pendidikan->pendidikan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $pendidikan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $pendidikan->updated_at }}</p>
</div>

