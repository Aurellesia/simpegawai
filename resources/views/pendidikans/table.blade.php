<div class="table-responsive-sm">
    <table class="table table-striped" id="pendidikans-table">
        <thead>
            <tr>
                <th>Pendidikan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pendidikans as $pendidikan)
            <tr>
                <td>{{ $pendidikan->pendidikan }}</td>
                <td>
                    {!! Form::open(['route' => ['pendidikans.destroy', $pendidikan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pendidikans.show', [$pendidikan->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('pendidikans.edit', [$pendidikan->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>