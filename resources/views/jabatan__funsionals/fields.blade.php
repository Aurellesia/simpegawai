<!-- Jabatan Fungsional Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jabatan_fungsional', 'Jabatan Fungsional:') !!}
    {!! Form::text('jabatan_fungsional', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jabatanFunsionals.index') }}" class="btn btn-secondary">Cancel</a>
</div>
