<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatanFunsionals-table">
        <thead>
            <tr>
                <th>Jabatan Fungsional</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jabatanFunsionals as $jabatanFunsional)
            <tr>
                <td>{{ $jabatanFunsional->jabatan_fungsional }}</td>
                <td>
                    {!! Form::open(['route' => ['jabatanFunsionals.destroy', $jabatanFunsional->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('jabatanFunsionals.show', [$jabatanFunsional->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jabatanFunsionals.edit', [$jabatanFunsional->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>