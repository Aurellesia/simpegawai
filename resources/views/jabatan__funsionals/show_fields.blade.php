<!-- Jabatan Fungsional Field -->
<div class="form-group">
    {!! Form::label('jabatan_fungsional', 'Jabatan Fungsional:') !!}
    <p>{{ $jabatanFunsional->jabatan_fungsional }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $jabatanFunsional->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $jabatanFunsional->updated_at }}</p>
</div>

