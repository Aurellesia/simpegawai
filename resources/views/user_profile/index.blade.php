@extends('layouts.app')
<style>
    label {
        color: grey;
    }
</style>
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-header">
                        <strong>Details</strong>
                    </div>
                    <div class="card-body">
                        <div class="card-body">
                            <center>
                                <!-- Foto Field -->
                                <div class="form-group">
                                    <img width="150px" src="#urlFoto">
                                </div>
                                <center>
                                    <div class="row">
                                        @include('user_profile.show_profile')
                                    </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
