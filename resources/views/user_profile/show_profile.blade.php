@extends('layouts.app')
<style>
    label {
        color: grey;
    }
</style>
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ route('pegawais.index') }}">Pegawai</a>
    </li>
    <li class="breadcrumb-item active">Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @include('coreui-templates::common.errors')
        <div class="row">
            <div class="col-lg-12">

                @foreach ($pegawai as $pegawai)
                <div class="card">
                    <div class="card-header">
                        <strong>Details</strong>
                        <a href="{{ route('pegawais.index') }}" class="btn btn-light">Back</a>
                    </div>
                    <div class="card-body">
                        <center>
                            <!-- Foto Field -->
                            <div class="form-group">
                                <img width="150px" src="{{ url('/data_file/img/'.$pegawai->foto) }}">
                            </div>
                            <center>
                                <div class="row">
                                    @include('pegawais.show_fields')
                                </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection