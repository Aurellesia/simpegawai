<div class="table-responsive-sm">
    <table class="table table-striped" id="absensi-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Kehadiran</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($absensiPegawais as $absensiPegawai)
            <tr>
                <td>{{ $absensiPegawai->tanggal }}</td>
                <td>{{ $absensiPegawai->kehadiran }}</td>
                <td>{{ $absensiPegawai->keterangan }}</td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>