<!-- Id Pegawai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    {!! Form::select('id_pegawai', $pegawaiItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Id Struktural Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_struktural', 'Id Struktural:') !!}
    {!! Form::select('id_struktural', $jabatan__strukturalItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Id Fungsional Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_fungsional', 'Id Fungsional:') !!}
    {!! Form::select('id_fungsional', $jabatan__funsionalItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Id Tambahan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tambahan', 'Id Tambahan:') !!}
    {!! Form::select('id_tambahan', $jabatan__tambahanItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jabatanPegawais.index') }}" class="btn btn-secondary">Cancel</a>
</div>
