<div class="table-responsive-sm">
    <table class="table table-striped" id="jabatanPegawais-table">
        <thead>
            <tr>
                <th>Id Pegawai</th>
        <th>Id Struktural</th>
        <th>Id Fungsional</th>
        <th>Id Tambahan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jabatanPegawais as $jabatanPegawai)
            <tr>
                <td>{{ $jabatanPegawai->id_pegawai }}</td>
            <td>{{ $jabatanPegawai->struktural->jabatan_struktural }}</td>
            <td>{{ $jabatanPegawai->fungsional->jabatan_fungsional }}</td>
            <td>{{ $jabatanPegawai->tambahan->jabatan_tambahan }}</td>
                <td>
                    {!! Form::open(['route' => ['jabatanPegawais.destroy', $jabatanPegawai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('jabatanPegawais.show', [$jabatanPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jabatanPegawais.edit', [$jabatanPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
