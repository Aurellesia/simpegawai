<!-- Id Pegawai Field -->
<div class="form-group">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    <p>{{ $jabatanPegawai->id_pegawai }}</p>
</div>

<!-- Id Struktural Field -->
<div class="form-group">
    {!! Form::label('id_struktural', 'Id Struktural:') !!}
    <p>{{ $jabatanPegawai->id_struktural }}</p>
</div>

<!-- Id Fungsional Field -->
<div class="form-group">
    {!! Form::label('id_fungsional', 'Id Fungsional:') !!}
    <p>{{ $jabatanPegawai->id_fungsional }}</p>
</div>

<!-- Id Tambahan Field -->
<div class="form-group">
    {!! Form::label('id_tambahan', 'Id Tambahan:') !!}
    <p>{{ $jabatanPegawai->id_tambahan }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $jabatanPegawai->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $jabatanPegawai->updated_at }}</p>
</div>

