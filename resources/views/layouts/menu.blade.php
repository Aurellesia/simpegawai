
<li class="nav-item {{ Request::is('pegawais*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('pegawais.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Pegawai</span>
    </a>
</li>
<li class="nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Master</span>
    </a>
    <ul class="nav-dropdown-items">
        <li class="nav-item {{ Request::is('agamas*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('agamas.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Agama</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('cutis*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('cutis.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Cuti</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('diklats*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('diklats.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Diklat</span>
            </a>
        </li>

        <li class="nav-item {{ Request::is('gapoks*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('gapoks.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Gaji Pokok</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('jabatanStrukturals*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jabatanStrukturals.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Struktural</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('jabatanFunsionals*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jabatanFunsionals.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Funsional</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('jabatanTambahans*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jabatanTambahans.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan  Tambahan</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('pendidikans*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('pendidikans.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Pendidikan</span>
            </a>
        </li>
        <li class="nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <span>Master Pelanggaran</span>
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item {{ Request::is('pelanggarans*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('pelanggarans.index') }}">
                        <i class="nav-icon icon-cursor"></i>
                        <span>Pelanggaran</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('tindakans*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('tindakans.index') }}">
                        <i class="nav-icon icon-cursor"></i>
                        <span>Tindakan</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li class="nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="nav-icon icon-cursor"></i>
        <span>Data Transaksi</span>
    </a>
    <ul class="nav-dropdown-items">
        <li class="nav-item {{ Request::is('pelanggaranPegawais*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('pelanggaranPegawais.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Pelanggaran  Pegawai</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('absensis*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('absensis.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Absensis</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('penggajianPegawais*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('penggajianPegawais.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Penggajian Pegawais</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('jabatanPegawais*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jabatanPegawais.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Jabatan Pegawais</span>
            </a>
        </li>

        <li class="nav-item {{ Request::is('diklatPegawais*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('diklatPegawais.index') }}">
                <i class="nav-icon icon-cursor"></i>
                <span>Diklat Pegawais</span>
            </a>
        </li>
    </ul>
</li>

