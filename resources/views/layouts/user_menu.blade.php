
<!-- Profile -->
<li class="nav-item {{ Request::is('user_profile*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_profile.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Profil Saya</span>
    </a>
</li>

<!-- absensi -->
<li class="nav-item {{ Request::is('user_absensi*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_absensi.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Rekap Absensi</span>
    </a>
</li>

<!-- gaji -->
<li class="nav-item {{ Request::is('user_gaji*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_gaji.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Gaji</span>
    </a>
</li>

<!-- pelanggaran -->
<li class="nav-item {{ Request::is('user_pelanggaran*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_pelanggaran.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Pelanggaran</span>
    </a>
</li>

<!-- jabatan -->
<li class="nav-item {{ Request::is('user_jabatan*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_jabatan.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Jabatan</span>
    </a>
</li>

<!-- diklat -->
<li class="nav-item {{ Request::is('user_diklat*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_diklat.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Diklat</span>
    </a>
</li>

<!-- cuti -->
<li class="nav-item {{ Request::is('user_cuti*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('user_cuti.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Cuti</span>
    </a>
</li>