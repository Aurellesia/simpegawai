<!-- Id Pegawai Field -->
<div class="form-group">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    <p>{{ $penggajianPegawai->id_pegawai }}</p>
</div>

<!-- Gapok Field -->
<div class="form-group">
    {!! Form::label('gapok', 'Gapok:') !!}
    <p>{{ $penggajianPegawai->gapok }}</p>
</div>

<!-- Gaji Tambahan Field -->
<div class="form-group">
    {!! Form::label('gaji_tambahan', 'Gaji Tambahan:') !!}
    <p>{{ $penggajianPegawai->gaji_tambahan }}</p>
</div>

<!-- Gaji Total Field -->
<div class="form-group">
    {!! Form::label('gaji_total', 'Gaji Total:') !!}
    <p>{{ $penggajianPegawai->gaji_total }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $penggajianPegawai->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $penggajianPegawai->updated_at }}</p>
</div>

