<!-- Id Pegawai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pegawai', 'Id Pegawai:') !!}
    {!! Form::select('id_pegawai', $pegawaiItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Gapok Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gapok', 'Gapok:') !!}
    {!! Form::select('gapok', $gapokItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Gaji Tambahan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gaji_tambahan', 'Gaji Tambahan:') !!}
    {!! Form::number('gaji_tambahan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('penggajianPegawais.index') }}" class="btn btn-secondary">Cancel</a>
</div>
