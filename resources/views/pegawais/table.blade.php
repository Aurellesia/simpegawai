<div class="table-responsive-sm">
    <table class="table table-striped" id="pegawais-table">
        <thead>
            <tr>
                <th>Nip</th>
        <th>Nama</th>
        <th>Telp</th>
        <th>Jenis Kelamin</th>
        <th>Status Pegawai</th>
        <th>Foto</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pegawais as $pegawai)
            <tr>
                <td>{{ $pegawai->nip }}</td>
            <td>{{ $pegawai->gelar_depan }} {{ $pegawai->nama }} {{ $pegawai->gelar_belakang }}</td>
            <td>{{ $pegawai->telp }}</td>
            <td>{{ $pegawai->jenis_kelamin }}</td>
            <td>{{ $pegawai->status_pegawai }}</td>
            <td><img width="150px" src="{{ url('/data_file/img/'.$pegawai->foto) }}"></td>
                <td>
                    {!! Form::open(['route' => ['pegawais.destroy', $pegawai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pegawais.show', [$pegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('pegawais.edit', [$pegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
