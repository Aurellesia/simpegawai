<div class="col-md-3">
    <!-- Nip Field -->
    <div class="form-group">
        {!! Form::label('nip', 'Nip:') !!}
        <p>{{ $pegawai->nip }}</p>
    </div>

    <!-- Nama Field -->
    <div class="form-group">
        {!! Form::label('nama', 'Nama:') !!}
        <p>{{ $pegawai->gelar_depan }} {{ $pegawai->nama }} {{ $pegawai->gelar_belakang }}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        <p>{{ $pegawai->email }}</p>
    </div>

    <!-- Password Field -->
    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        <p>{{ $pegawai->password }}</p>
    </div>
    <!-- Telp Field -->
    <div class="form-group">
        {!! Form::label('telp', 'Telp:') !!}
        <p>{{ $pegawai->telp }}</p>
    </div>
</div>

<div class="col-md-3">
    <!-- Tempat Field -->
    <div class="form-group">
        {!! Form::label('tempat', 'Tempat/Tgl Lahir:') !!}
        <p>{{ $pegawai->tempat }}/{{ $pegawai->tgl_lahir }}</p>
    </div>


<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $pegawai->jenis_kelamin }}</p>
</div>

<!-- Status Pegawai Field -->
<div class="form-group">
    {!! Form::label('status_pegawai', 'Status Pegawai:') !!}
    <p>{{ $pegawai->status_pegawai }}</p>
</div>
<!-- Id Agama Field -->
<div class="form-group">
    {!! Form::label('id_agama', 'Agama:') !!}
    <p>{{ $pegawai->agama->agama }}</p>
</div>
</div>
<div class="col-md-3">


<!-- Hobi Field -->
<div class="form-group">
    {!! Form::label('hobi', 'Hobi:') !!}
    <p>{{ $pegawai->hobi }}</p>
</div>

<!-- Id Pendidikan Field -->
<div class="form-group">
    {!! Form::label('id_pendidikan', 'Pendidikan Terakhir:') !!}
    <p>{{ $pegawai->pendidikan->pendidikan }}</p>
</div>

<!-- Nama Sekolah Field -->
<div class="form-group">
    {!! Form::label('nama_sekolah', 'Nama Sekolah:') !!}
    <p>{{ $pegawai->nama_sekolah }}</p>
</div>
<!-- Tahun Sttb Field -->
    <div class="form-group">
        {!! Form::label('tahun_sttb', 'Tahun Sttb:') !!}
        <p>{{ $pegawai->tahun_sttb }}</p>
    </div>
</div>
<div class="col-md-3">
<!-- Status Pernikahan Field -->
    <div class="form-group">
        {!! Form::label('status_pernikahan', 'Status Pernikahan:') !!}
        <p>{{ $pegawai->status_pernikahan }}</p>
    </div>
    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', 'Tanggal Masuk:') !!}
        <p>{{ $pegawai->created_at }}</p>
    </div>
    <!-- Updated At Field -->
    <div class="form-group">
        {!! Form::label('updated_at', 'Updated At:') !!}
        <p>{{ $pegawai->updated_at }}</p>
    </div>
</div>

