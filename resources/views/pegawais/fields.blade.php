<!-- Nip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nip', 'Nip:') !!}
    {!! Form::text('nip', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Agama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_agama', 'Agama:') !!}
    {!! Form::select('id_agama', $agama, null, ['class' => 'form-control']) !!}
</div>
<!-- Tempat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempat', 'Tempat:') !!}
    {!! Form::text('tempat', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Lahir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    {!! Form::date('tgl_lahir', null, ['class' => 'form-control']) !!}
</div>

<!-- Telp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telp', 'Telp:') !!}
    {!! Form::number('telp', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group col-sm-12">
    <fieldset class="form-group">
        <div class="row">
            {!! Form::label('jenis_kelamin', 'Jenis Kelamin:',['class'=>'col-form-label col-sm-2 pt-0']) !!}
            <div class="col-sm-10">
                <label class="form-check form-check-label">
                    {!! Form::radio('jenis_kelamin', "laki-laki", null,['class'=>'form-check-input']) !!} laki-laki
                </label>

                <label class="form-check form-check-label">
                    {!! Form::radio('jenis_kelamin', "perempuan", null,['class'=>'form-check-input'])!!} perempuan
                </label>
            </div>
        </div>
    </fieldset>
</div>
<!-- Status Pegawai Field -->
<div class="form-group col-sm-12">
    <fieldset class="form-group">
        <div class="row">
            {!! Form::label('status_pegawai', 'Status Pegawai:',['class'=>'col-form-label col-sm-2 pt-0']) !!}
            <div class="col-sm-10">
                <label class="form-check form-check-label">
                     {!! Form::radio('status_pegawai', "aktif", null,['class'=>'form-check-input']) !!} aktif
                </label>

                <label class="form-check form-check-label">
                   {!! Form::radio('status_pegawai', "non_aktif", null,['class'=>'form-check-input']) !!} non_aktif
                </label>
            </div>
        </div>
    </fieldset>
</div>
<!-- Foto Field -->
<div class="custom-file">
    <div class="form-group col-sm-6">
        <div class="custom-file">
            {!! Form::label('foto', 'Foto:',['class'=>'custom-file-label']) !!}
            {!! Form::file('foto',null,['class'=>'custom-file-input'])!!}
        </div>
    </div>
<div class="clearfix"></div>

<!-- Hobi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hobi', 'Hobi:') !!}
    {!! Form::text('hobi', null, ['class' => 'form-control']) !!}
</div>
<!-- Jenis Pendidikan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pendidikan', 'Pendidikan:') !!}
    {!! Form::select('id_pendidikan', $pendidikan, null, ['class' => 'form-control']) !!}
</div>
<!-- Nama Sekolah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_sekolah', 'Nama Sekolah:') !!}
    {!! Form::text('nama_sekolah', null, ['class' => 'form-control']) !!}
</div>

<!-- Tahun Sttb Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tahun_sttb', 'Tahun Sttb:') !!}
    {!! Form::text('tahun_sttb', null, ['class' => 'form-control']) !!}
</div>

<!-- Gelar Depan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gelar_depan', 'Gelar Depan:') !!}
    {!! Form::text('gelar_depan', null, ['class' => 'form-control']) !!}
</div>

<!-- Gelar Belakang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gelar_belakang', 'Gelar Belakang:') !!}
    {!! Form::text('gelar_belakang', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Pernikahan Field -->
<div class="form-group col-sm-12">
    <fieldset class="form-group">
        <div class="row">
            {!! Form::label('status_pernikahan', 'Status Pernikahan:',['class'=>'col-form-label col-sm-2 pt-0']) !!}
            <div class="col-sm-10">
                <label class="form-check form-check-label">
                    {!! Form::radio('status_pernikahan', "Menikah", null,['class'=>'form-check-input']) !!} Menikah
                </label>

                <label class="form-check form-check-label">
                    {!! Form::radio('status_pernikahan', "Belum Menikah", null,['class'=>'form-check-input']) !!} Belum_Menikah
                </label>
            </div>
        </div>
    </fieldset>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pegawais.index') }}" class="btn btn-secondary">Cancel</a>
</div>
