@extends('layouts.app')
<style>
    label {
        color:grey;
       }
</style>
@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('pegawais.index') }}">Pegawai</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('pegawais.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                <center>
                                    <!-- Foto Field -->
                                    <div class="form-group">
                                        <img width="150px" src="{{ url('/data_file/img/'.$pegawai->foto) }}">
                                    </div>
                                <center>
                                <div class="row">
                                    @include('pegawais.show_fields')
                                 </div>
                            </div>
                         </div>
                         <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="jabatan-tab" data-toggle="tab" href="#jabatan" role="tab" aria-controls="jabatan" aria-selected="true">Jabatan</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="diklat-tab" data-toggle="tab" href="#diklat" role="tab" aria-controls="diklat" aria-selected="false">Diklat</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="hukuman-tab" data-toggle="tab" href="#hukuman" role="tab" aria-controls="hukuman" aria-selected="false">Hukuman</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="Gaji-tab" data-toggle="tab" href="#Gaji" role="tab" aria-controls="Gaji" aria-selected="false">Gaji</a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="jabatan" role="tabpanel" aria-labelledby="jabatan-tab">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped" id="jabatanPegawais-table">
                                        <thead>
                                            <tr>
                                                <th>Id Struktural</th>
                                                <th>Id Fungsional</th>
                                                <th>Id Tambahan</th>
                                                <th colspan="3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($jabatanPegawais as $jabatanPegawai)
                                            <tr>
                                                <td>{{ $jabatanPegawai->struktural->jabatan_struktural }}</td>
                                                <td>{{ $jabatanPegawai->fungsional->jabatan_fungsional }}</td>
                                                <td>{{ $jabatanPegawai->tambahan->jabatan_tambahan }}</td>
                                                <td>
                                                    {!! Form::open(['route' => ['jabatanPegawais.destroy', $jabatanPegawai->id], 'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a href="{{ route('jabatanPegawais.show', [$jabatanPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                        <a href="{{ route('jabatanPegawais.edit', [$jabatanPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="diklat" role="tabpanel" aria-labelledby="diklat-tab">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped" id="diklatPegawais-table">
                                        <thead>
                                            <tr>
                                                <th>Id Diklat</th>
                                                <th colspan="3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($diklatPegawais as $diklatPegawai)
                                            <tr>
                                                <td>{{$diklatPegawai->diklat->jenis_diklat }}</td>
                                                <td>
                                                    {!! Form::open(['route' => ['diklatPegawais.destroy', $diklatPegawai->id], 'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a href="{{ route('diklatPegawais.show', [$diklatPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                        <a href="{{ route('diklatPegawais.edit', [$diklatPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hukuman" role="tabpanel" aria-labelledby="hukuman-tab">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped" id="pelanggaranPegawais-table">
                                        <thead>
                                            <tr>
                                                <th>Pelanggaran</th>
                                                <th>Tindakan</th>
                                                <th>Keterangan</th>
                                                <th colspan="3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pelanggaranPegawais as $pelanggaranPegawai)
                                            <tr>
                                                <td>{{ $pelanggaranPegawai->Pelanggaran->pelanggaran }}</td>
                                                <td>{{ $pelanggaranPegawai->Tindakan->tindakan }}</td>
                                                <td>{{ $pelanggaranPegawai->keterangan }}</td>
                                                <td>
                                                    {!! Form::open(['route' => ['pelanggaranPegawais.destroy', $pelanggaranPegawai->id], 'method' => 'delete']) !!}
                                                    <div class='btn-group'>
                                                        <a href="{{ route('pelanggaranPegawais.show', [$pelanggaranPegawai->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                        <a href="{{ route('pelanggaranPegawais.edit', [$pelanggaranPegawai->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="Gaji" role="tabpanel" aria-labelledby="Gaji-tab">
                                @foreach ( $penggajianPegawai as  $penggajianPegawai )
                                <!-- Gapok Field -->
                                <div class="form-group">
                                    {!! Form::label('gapok', 'Gapok:') !!}
                                    <p>{{ $penggajianPegawai->Gapok->nominal_gaji }}</p>
                                </div>

                                <!-- Gaji Tambahan Field -->
                                <div class="form-group">
                                    {!! Form::label('gaji_tambahan', 'Gaji Tambahan:') !!}
                                    <p>{{ $penggajianPegawai->gaji_tambahan }}</p>
                                </div>

                                <!-- Gaji Total Field -->
                                <div class="form-group">
                                    {!! Form::label('gaji_total', 'Gaji Total:') !!}
                                    <p>{{ $penggajianPegawai->gaji_total }}</p>
                                </div>
                                @endforeach
                            </div>
                          </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
