<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->middleware('verified');
Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {
    Route::resource('agamas', 'AgamaController');
    Route::resource('diklats', 'DiklatController');
    Route::resource('gapoks', 'GapokController');
    Route::resource('jabatanStrukturals', 'Jabatan_StrukturalController');
    Route::resource('jabatanFunsionals', 'Jabatan_FunsionalController');
    Route::resource('jabatanTambahans', 'Jabatan_TambahanController');
    Route::resource('pendidikans', 'PendidikanController');
    Route::resource('pegawais', 'PegawaiController');
    Route::resource('pelanggarans', 'PelanggaranController');
    Route::resource('tindakans', 'TindakanController');
    Route::resource('pelanggaranPegawais', 'Pelanggaran_PegawaiController');
    Route::resource('absensis', 'AbsensiController');
    Route::resource('cutis','CutiController');
    Route::get('absensis/cari', function () {
        return view('welcome');
    });
    Route::resource('penggajianPegawais', 'PenggajianPegawaiController');
    Route::resource('jabatanPegawais', 'JabatanPegawaiController');
    Route::resource('diklatPegawais', 'DiklatPegawaiController');
    Route::get('admin/cuti','UserController@cutiAccTampil');
    Route::post('admin/cuti/acc','UserController@cutiAccUpdate');

});
// user

Route::group(['middleware' => ['auth', 'checkRole:pegawai']], function () {
    Route::get('/users', ['as' => 'user_profile.index', 'uses' => 'UserController@profile']);
    Route::get('/users/profil', ['as' => 'user_profile.index', 'uses' => 'UserController@profile']);
    Route::get('/users/absensi', ['as' => 'user_absensi.index', 'uses' => 'UserController@absensi']);
    Route::get('/users/gaji', ['as' => 'user_gaji.index', 'uses' => 'UserController@gaji']);
    Route::get('/users/pelanggaran', ['as' => 'user_pelanggaran.index', 'uses' => 'UserController@pelanggaran']);
    Route::get('/users/jabatan', ['as' => 'user_jabatan.index', 'uses' => 'UserController@jabatan']);
    Route::get('/users/diklat', ['as' => 'user_diklat.index', 'uses' => 'UserController@diklat']);
    Route::get('/users/cuti', ['as' => 'user_cuti.index', 'uses' => 'UserController@cuti']);
    Route::get('/users/cuti/create', ['as' => 'user_cuti.create', 'uses' => 'UserController@cutiCreate']);
    Route::post('/users/cuti/store', ['as' => 'user_cuti.store', 'uses' => 'UserController@cutiStore']);
    Route::get('/users/cuti/show/{id}', ['as' => 'user_cuti.show', 'uses' => 'UserController@cutiShow']);
    Route::get('/users/cuti/edit/{id}', ['as' => 'user_cuti.edit', 'uses' => 'UserController@cutiEdit']);
    Route::patch('/users/cuti/update/{id}', ['as' => 'user_cuti.update', 'uses' => 'UserController@cutiUpdate']);
    Route::delete('/users/cuti/destroy/{id}', ['as' => 'user_cuti.destroy', 'uses' => 'UserController@cutiDestroy']);
});
